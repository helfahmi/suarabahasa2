$(function() {
  /* submenu utility functions */
  $("#submenu-new").click(function() {
    if (!$("#submenu-new").hasClass("active")) {
      $("#submenu-new").toggleClass("active");
      if ($("#submenu-drafts").hasClass("active")) {
        $("#collapse-draft").collapse("hide");
        
        $("#nav-drop-arrow-drafts").addClass("glyphicon-chevron-down");
        $("#nav-drop-arrow-drafts").removeClass("glyphicon-chevron-up");
    
        $("#submenu-drafts").toggleClass("active");
      } else {
        $("#collapse-recent").collapse("hide");
        
        $("#nav-drop-arrow-recents").addClass("glyphicon-chevron-down");
        $("#nav-drop-arrow-recents").removeClass("glyphicon-chevron-up");
    
        $("#submenu-recents").toggleClass("active");
      }
    }
  });
  
  $("#submenu-drafts").click(function() {
    if (!$("#submenu-drafts").hasClass("active")) {
      $("#submenu-drafts").toggleClass("active");
      if ($("#submenu-recents").hasClass("active")) {
        $("#collapse-recent").collapse("hide");
        
        $("#nav-drop-arrow-recents").addClass("glyphicon-chevron-down");
        $("#nav-drop-arrow-recents").removeClass("glyphicon-chevron-up");
        
        $("#submenu-recents").toggleClass("active");
      } else {
        $("#submenu-new").toggleClass("active");
      }
    }
  });
  
  $("#submenu-recents").click(function() {
    if (!$("#submenu-recents").hasClass("active")) {
      $("#submenu-recents").toggleClass("active");
      if ($("#submenu-new").hasClass("active")) {
        $("#submenu-new").toggleClass("active");
      } else {
        $("#collapse-draft").collapse("hide");
        
        $("#nav-drop-arrow-drafts").addClass("glyphicon-chevron-down");
        $("#nav-drop-arrow-drafts").removeClass("glyphicon-chevron-up");
        
        $("#submenu-drafts").toggleClass("active");
      }
    }
  });

  $("#nav-drop-drafts").click(function() {
    $("#nav-drop-arrow-drafts").toggleClass("glyphicon-chevron-down");
    $("#nav-drop-arrow-drafts").toggleClass("glyphicon-chevron-up");
  });
  
  $("#nav-drop-recents").click(function() {
    $("#nav-drop-arrow-recents").toggleClass("glyphicon-chevron-down");
    $("#nav-drop-arrow-recents").toggleClass("glyphicon-chevron-up");
  });
});