$(function() {
	$('#theme1').collapse('toggle');
	$('#theme2').collapse('toggle');
	$('#theme3').collapse('toggle');
	$('#theme4').collapse('toggle');
	$('#theme5').collapse('toggle');
	$('#theme6').collapse('toggle');
	$('#theme7').collapse('toggle');
	$('#theme8').collapse('toggle');

	$('#topic1').click(function() {
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').toggleClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic2').click(function() {
		$('#theme1').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').toggleClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic3').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').toggleClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic4').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').toggleClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic5').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').toggleClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic6').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme7').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').toggleClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic7').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme8').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').toggleClass('topictitle-active');
		$('#topictitle8').removeClass('topictitle-active');
	});

	$('#topic8').click(function() {
		$('#theme1').collapse('hide');
		$('#theme2').collapse('hide');
		$('#theme3').collapse('hide');
		$('#theme4').collapse('hide');
		$('#theme5').collapse('hide');
		$('#theme6').collapse('hide');
		$('#theme7').collapse('hide');

		$('#topictitle1').removeClass('topictitle-active');
		$('#topictitle2').removeClass('topictitle-active');
		$('#topictitle3').removeClass('topictitle-active');
		$('#topictitle4').removeClass('topictitle-active');
		$('#topictitle5').removeClass('topictitle-active');
		$('#topictitle6').removeClass('topictitle-active');
		$('#topictitle7').removeClass('topictitle-active');
		$('#topictitle8').toggleClass('topictitle-active');
	});
});