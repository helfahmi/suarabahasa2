<?php
/**
 * Register form model
 */
class RegisterForm extends CFormModel
{
  public $username;
  public $password;
  public $password_repeat;
  public $full_name;
  public $ttl;
  public $email;
  public $picture;

  public function rules()
  {
    return array(
      array('username, password, password_repeat, full_name, ttl, email', 'required'),
      array('email', 'email'),
      array('username, password, password_repeat', 'length', 'max'=>255),
      array('username, password, password_repeat', 'length', 'min'=>6),
      array('username', 'match', 'allowEmpty' => false, 'pattern' => '/[A-Za-z0-9\x80-\xFF]+$/'),
      array('username', 'unique', 'className' => 'User' ),
      array('picture', 'file', 'allowEmpty' => true, 'types' => 'jpg, png, gif, bmp', 'maxSize' => 1024 * 1024 * 5),
      array('password', 'compare', 'compareAttribute'=>'password_repeat'),
    );
  }

  public function attributeLabels()
  {
    return array(
      'username' => Yii::t('user', 'Username'),
      'password' => Yii::t('user', 'Password'),
      'password_repeat' => Yii::t('user', 'Repeat Password'),
      'full_name' => Yii::t('user', 'Full Name'),
      'picture' => Yii::t('user', 'Profile Picture'),
      'ttl' => Yii::t('user', 'Birth'),
      'email' => Yii::t('user', 'Email')
    );
  }
}