<?php

class PostListens extends PostListenBase
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTitle($id){
        return PostListens::model()->findByPk($id)->title;
    }

    public static function getLike($id){
        return PostListens::model()->findByPk($id)->likes;
    }

    public static function getDislike($id){
        return PostListens::model()->findByPk($id)->dislikes;
    }

    public static function getDate($id){
        return PostListens::model()->findByPk($id)->date;
    }

    public static function getZamrud($id){
        return PostListens::model()->findByPk($id)->zamrud_points;
    }

    public static function getLang($id){
    	return Lang::getLangName(PostListens::model()->findByPk($id)->lang_id);
    }

    public static function getUser($id){
    	return User::getFullName(PostListens::model()->findByPk($id)->user_id);
    }
}
