<?php

class Quiz extends QuizBase
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getTakersCount($post_listen_id){
		$takers = Quiz::model()->find('post_listen_id=:post_listen_id', array(':post_listen_id'=>$post_listen_id));
		return count($takers);
	}
}
