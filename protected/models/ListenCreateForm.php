<?php 

 
 class ListenCreateForm extends CFormModel
 {

 	public $title;
 	public $lang_id;
	public $upload_file;

	
	public function rules()
	{
		return array(
			// username and password are required
			array('title,upload_file', 'required','message'=>'Fill the tittle & Record Your Video First !'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'lang_id'=>'Language',
		);
	}
	
	public function getangs()
        {
               
            $dataProvider=new CActiveDataProvider('Permit');//,array('criteria'=>$criteria));
            $data=$dataProvider->getData();
            
            $return=array();
            
            foreach($data as $s)
            {
                $return[$s->siteID] = $s->siteName;
            }
            
            return $return;
        }
	
	
 }
 
?>