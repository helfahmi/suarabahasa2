<?php

class User extends UserBase
{

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function validatePassword($password)
    {
        return $this->hashPassword($password,$this->salt)===$this->password;
    }

    public function hashPassword($password,$salt)
    {
        return md5($salt.$password);
    }

    public function generateSalt()
    {
        return uniqid('',true);
    }

    public static function getNativeLang($id){
    	return Lang::getLangName(User::model()->findByPk($id)->native_lang_id);
    }

    public static function getLearnLang($id){
    	return Lang::getLangName(User::model()->findByPk($id)->learn_lang_id);
    }

    public static function getFullName($id){
        return User::model()->findByPk($id)->full_name;
    }

    public static function getUsername($id){
        return User::model()->findByPk($id)->username;
    }

    public static function getPicture($id){
        return User::model()->findByPk($id)->picture;
    }

    public static function getTtl($id){
        return User::model()->findByPk($id)->ttl;
    }
}
