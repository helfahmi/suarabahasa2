<?php 
/**
 * PostForm class.
 * PostForm is the data structure for keeping
 * user post form data. It is used by the 'Create' action of 'SiteController'.
 */
 
 class PostForm extends CFormModel
 {
	public $title;
	public $lang_id;
	public $video_file;
	public $content;
	public $upload_file;
	
	public function rules()
	{
		return array(
			// username and password are required
			array('title, lang_id', 'required'), //some error if video_file required -_- <-- so, debug it
			array('video_file', 'file', 'allowEmpty'=>true, 'types' => 'webm, mp4, flv', 'maxSize' => 1024 * 1024 * 50),
			array('upload_file','either','other'=>'video_file'), 
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'title' => Yii::t('labels', 'Title'),
			'lang_id' => Yii::t('labels', 'Language'),
			'video_file' => Yii::t('labels', 'Video'),
			'content' => Yii::t('labels', 'Description'),
		);
	}
	
	public function getangs()
        {
            
            
            $dataProvider=new CActiveDataProvider('Permit');//,array('criteria'=>$criteria));
            $data=$dataProvider->getData();
            
            $return=array();
            
            foreach($data as $s)
            {
                $return[$s->siteID] = $s->siteName;
            }
            
            return $return;
        }
	
	public function either($attribute_name, $params)
	{
	    $field1 = $this->getAttributeLabel($attribute_name);
	   	$field2 = $this->getAttributeLabel($params['other']);
	    if ((empty($this->$attribute_name) && (CUploadedFile::getInstance($this, 'video_file') == null) ) || (!empty($this->$attribute_name) && (CUploadedFile::getInstance($this, 'video_file') != null) )) {

			$this->addError($attribute_name, Yii::t('user', "either {$field1} or {$field2} is required."));
	        return false;
	    }
	    return true;
	}

 }
 
?>