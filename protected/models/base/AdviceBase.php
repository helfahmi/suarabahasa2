<?php

/**
 * This is the model class for table "advices".
 *
 * The followings are the available columns in table 'advices':
 * @property integer $id
 * @property integer $user_id
 * @property integer $post_id
 * @property integer $parent_id
 * @property string $title
 * @property string $content
 * @property integer $zamrud_points
 * @property string $date
 * @property integer $upvote
 * @property integer $downvote
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Posts $post
 * @property AdviceBase $parent
 * @property AdviceBase[] $advices
 */
class AdviceBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, post_id, parent_id, zamrud_points, upvote, downvote', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			array('content, date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, post_id, parent_id, title, content, zamrud_points, date, upvote, downvote', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'post' => array(self::BELONGS_TO, 'Posts', 'post_id'),
			'parent' => array(self::BELONGS_TO, 'AdviceBase', 'parent_id'),
			'advices' => array(self::HAS_MANY, 'AdviceBase', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'post_id' => 'Post',
			'parent_id' => 'Parent',
			'title' => 'Title',
			'content' => 'Content',
			'zamrud_points' => 'Zamrud Points',
			'date' => 'Date',
			'upvote' => 'Upvote',
			'downvote' => 'Downvote',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('zamrud_points',$this->zamrud_points);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('upvote',$this->upvote);
		$criteria->compare('downvote',$this->downvote);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdviceBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
