<?php

/**
 * This is the model class for table "post_listens".
 *
 * The followings are the available columns in table 'post_listens':
 * @property integer $id
 * @property integer $user_id
 * @property integer $lang_id
 * @property integer $theme_id
 * @property integer $topic_id
 * @property integer $story_id
 * @property string $title
 * @property integer $zamrud_points
 * @property string $date
 * @property integer $status
 * @property integer $likes
 * @property integer $dislikes
 */
class PostListenBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post_listens';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, lang_id, theme_id, topic_id, story_id, title, zamrud_points, date, status', 'required'),
			array('user_id, lang_id, theme_id, topic_id, story_id, zamrud_points, status, likes, dislikes', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, lang_id, theme_id, topic_id, story_id, title, zamrud_points, date, status, likes, dislikes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'lang_id' => 'Lang',
			'theme_id' => 'Theme',
			'topic_id' => 'Topic',
			'story_id' => 'Story',
			'title' => 'Title',
			'zamrud_points' => 'Zamrud Points',
			'date' => 'Date',
			'status' => 'Status',
			'likes' => 'Likes',
			'dislikes' => 'Dislikes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('theme_id',$this->theme_id);
		$criteria->compare('topic_id',$this->topic_id);
		$criteria->compare('story_id',$this->story_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('zamrud_points',$this->zamrud_points);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('likes',$this->likes);
		$criteria->compare('dislikes',$this->dislikes);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PostListenBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
