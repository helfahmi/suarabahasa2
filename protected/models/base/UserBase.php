<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $full_name
 * @property string $name
 * @property string $ttl
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $last_visit
 * @property string $join_date
 * @property string $picture
 * @property integer $green_zamrud
 * @property integer $red_zamrud
 * @property integer $disciple_caste_id
 * @property integer $master_caste_id
 * @property integer $native_lang_id
 * @property integer $learn_lang_id
 * @property string $role
 * @property string $fb_id
 * @property string $gender
 *
 * The followings are the available model relations:
 * @property Advices[] $advices
 * @property Messages[] $messages
 * @property Messages[] $messages1
 * @property Posts[] $posts
 * @property Langs $learnLang
 * @property Langs $nativeLang
 * @property Castes $discipleCaste
 * @property Castes $masterCaste
 * @property Videos[] $videoses
 */
class UserBase extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('full_name, email, username, password', 'required'),
			array('green_zamrud, red_zamrud, disciple_caste_id, master_caste_id, native_lang_id, learn_lang_id', 'numerical', 'integerOnly'=>true),
			array('full_name', 'length', 'max'=>100),
			array('name, email, username, password, salt, picture, role, fb_id, gender', 'length', 'max'=>45),
			array('ttl, last_visit, join_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, full_name, name, ttl, email, username, password, salt, last_visit, join_date, picture, green_zamrud, red_zamrud, disciple_caste_id, master_caste_id, native_lang_id, learn_lang_id, role, fb_id, gender', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'advices' => array(self::HAS_MANY, 'Advices', 'user_id'),
			'messages' => array(self::HAS_MANY, 'Messages', 'receiver_id'),
			'messages1' => array(self::HAS_MANY, 'Messages', 'sender_id'),
			'posts' => array(self::HAS_MANY, 'Posts', 'user_id'),
			'learnLang' => array(self::BELONGS_TO, 'Langs', 'learn_lang_id'),
			'nativeLang' => array(self::BELONGS_TO, 'Langs', 'native_lang_id'),
			'discipleCaste' => array(self::BELONGS_TO, 'Castes', 'disciple_caste_id'),
			'masterCaste' => array(self::BELONGS_TO, 'Castes', 'master_caste_id'),
			'videoses' => array(self::HAS_MANY, 'Videos', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'full_name' => 'Full Name',
			'name' => 'Name',
			'ttl' => 'Ttl',
			'email' => 'Email',
			'username' => 'Username',
			'password' => 'Password',
			'salt' => 'Salt',
			'last_visit' => 'Last Visit',
			'join_date' => 'Join Date',
			'picture' => 'Picture',
			'green_zamrud' => 'Green Zamrud',
			'red_zamrud' => 'Red Zamrud',
			'disciple_caste_id' => 'Disciple Caste',
			'master_caste_id' => 'Master Caste',
			'native_lang_id' => 'Native Lang',
			'learn_lang_id' => 'Learn Lang',
			'role' => 'Role',
			'fb_id' => 'Fb',
			'gender' => 'Gender',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('ttl',$this->ttl,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('last_visit',$this->last_visit,true);
		$criteria->compare('join_date',$this->join_date,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('green_zamrud',$this->green_zamrud);
		$criteria->compare('red_zamrud',$this->red_zamrud);
		$criteria->compare('disciple_caste_id',$this->disciple_caste_id);
		$criteria->compare('master_caste_id',$this->master_caste_id);
		$criteria->compare('native_lang_id',$this->native_lang_id);
		$criteria->compare('learn_lang_id',$this->learn_lang_id);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('fb_id',$this->fb_id,true);
		$criteria->compare('gender',$this->gender,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserBase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
