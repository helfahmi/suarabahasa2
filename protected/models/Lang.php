<?php

class Lang extends LangBase
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getLangName($id){
		return Lang::model()->findByPk($id)->name;
	}
}
