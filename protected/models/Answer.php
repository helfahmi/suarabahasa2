<?php
/**
 * Register form model
 */
class Answer extends CFormModel
{
  public $answer1;
  public $answer2;
  public $answer3;
  public $answer4;
 
  public function rules(){
    return array(
      array('answer1, answer2, answer3, answer4', 'required'),
    );
  }

  public function attributeLabels(){
    return array(
      'answer1' => "Answer 1",
      'answer2' => "Answer 2",
      'answer3' => "Answer 3",
      'answer4' => "Answer 4"
    );
  }
}