<?php

class VideoListens extends VideoListenBase
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getVideo($post_listen_id){
		$video = VideoListens::model()->findByPk($post_listen_id);
		return $video->file;
	}

	public static function getThumbnail($id){
		$video = VideoListens::model()->findByPk($id);
		return $video->thumbnail;
	}
}
