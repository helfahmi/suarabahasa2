<?php
  /* @var $this PostController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Quiz Maker";
?>

<div class="row" id="pagetitle">
  <div class="col-lg-12">
    <h1><?= Yii::t('labels', 'Listening Tutorial Topic') ?></h1>
  </div>
</div><!-- pagetitle -->
<div class="row" id="pagesubtitle">
  <div class="col-lg-12">
    <h2>Step 1: Pick a topic</h2>
  </div>
</div><!-- pagesubtitle -->
<div class="row" id="pagecontent">
  <div id="topic">
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme1" id="topic1">
          <img class="img-responsive img-rounded topic-card-green" src="<?= Yii::app()->request->baseUrl?>/res/img/culinary.png" width="100%" >
        </a>
        <h4 id="topictitle1">Traditional Food </h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme2" id="topic2">
          <img class="img-responsive img-rounded topic-card-blue" src="<?= Yii::app()->request->baseUrl?>/res/img/art.png" width="100%" >
        </a>
        <h4 id="topictitle2">Local Art</h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme3" id="topic3">
          <img class="img-responsive img-rounded topic-card-orange" src="<?= Yii::app()->request->baseUrl?>/res/img/folklore.png" width="100%" >
        </a>
        <h4 id="topictitle3">Folklore</h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme4" id="topic4">
          <img class="img-responsive img-rounded topic-card-red" src="<?= Yii::app()->request->baseUrl?>/res/img/geography.png" width="100%" >
        </a>
        <h4 id="topictitle4">Geography</h4>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme1">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>4,'topic'=>7)); ?>" class="list-group-item">Indonesian Drink</a>
        <a href="<?= $this->createUrl('postListens/create',array('theme'=>4,'topic'=>8)); ?>" class="list-group-item">Indonesian Food</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme2">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>9)); ?>" class="list-group-item">Traditional Music</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>10)); ?>" class="list-group-item">Traditional Dance</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>11)); ?>" class="list-group-item">Traditional Craft</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme3">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>4,'topic'=>7)); ?>" class="list-group-item">Legend</a>
        <a href="<?= $this->createUrl('postListens/create',array('theme'=>4,'topic'=>8)); ?>" class="list-group-item">Myth</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme4">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>9)); ?>" class="list-group-item">Island</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>10)); ?>" class="list-group-item">Mountain</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>11)); ?>" class="list-group-item">Sea & River</a>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme5" id="topic5">
          <img class="img-responsive img-rounded topic-card-purple" src="<?= Yii::app()->request->baseUrl?>/res/img/politic.png" width="100%" >
        </a>
        <h4 id="topictitle5">Politic</h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme6" id="topic6">
          <img class="img-responsive img-rounded topic-card-yellow" src="<?= Yii::app()->request->baseUrl?>/res/img/sport.png" width="100%" >
        </a>
        <h4 id="topictitle6">Sport</h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme7" id="topic7">
          <img class="img-responsive img-rounded topic-card-brown" src="<?= Yii::app()->request->baseUrl?>/res/img/history.png" width="100%" >
        </a>
        <h4 id="topictitle7">History</h4>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="text-center">
        <a data-toggle="collapse" data-parent="#topic" href="#theme8" id="topic8">
          <img class="img-responsive img-rounded topic-card-olive" src="<?= Yii::app()->request->baseUrl?>/res/img/tourism.png" width="100%" >
        </a>
        <h4 id="topictitle8">Tourism</h4>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme5">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>4,'topic'=>7)); ?>" class="list-group-item">Presiden</a>
        <a href="<?= $this->createUrl('postListens/create',array('theme'=>4,'topic'=>8)); ?>" class="list-group-item">Government</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme6">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>9)); ?>" class="list-group-item">Atlet</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>10)); ?>" class="list-group-item">Achievement</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme7">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>9)); ?>" class="list-group-item">Hero</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>10)); ?>" class="list-group-item">Palace</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>11)); ?>" class="list-group-item">Organization</a>
      </div>
    </div>
    <div class="col-lg-12 collapse in" id="theme8">
      <div class="list-group">
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>9)); ?>" class="list-group-item">Place</a>
        <a href="<?= $this->createUrl('postlistens/create',array('theme'=>5,'topic'=>10)); ?>" class="list-group-item">Souvenir</a>
      </div>
    </div>
  </div>
</div><!-- pagecontent -->

<?php
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/topicpicker.js', CClientScript::POS_END);
?>