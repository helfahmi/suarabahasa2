<?php
  /* @var $this SiteController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Video Posts";
?>

<head>
  <link href="http://vjs.zencdn.net/4.6/video-js.css" rel="stylesheet"> </link>
    <script src="http://vjs.zencdn.net/4.6/video.js"></script>
    <style type="text/css">
      .vjs-default-skin { color: #1b71a4; }
      .vjs-default-skin .vjs-play-progress,
      .vjs-default-skin .vjs-volume-level { background-color: #070808 }
      .vjs-default-skin .vjs-control-bar,
      .vjs-default-skin .vjs-big-play-button { background: rgba(232,226,226,0.7) }
      .vjs-default-skin .vjs-slider { background: rgba(232,226,226,0.2333333333333333) }
      .vjs-default-skin .vjs-control-bar { font-size: 127% }
    </style>
</head>

<!-- Page title -->
<div class="row">
  <div class="col-lg-12">
    <h2>Take Quiz by Watching Videos <small>Practice your targeted language </small><a href=""></a></h2>
  </div>
</div><!-- Page title -->

<!-- Gap -->
<div class="row">
  <div class="col-lg-12"></div>
</div><!-- Gap -->

<!-- Page content -->
<div class="row">
  <!-- Main content -->
  <div class="col-lg-11">

      <!-- Video container -->
      <div class="inner-panel">
        <ul class="media-list panel-comment">
          <!-- Video -->
          <?php
            foreach($models as $model) {
              //$model_advices = Advice::model()->findAll('post_listen_id = :post_listen_id', array(':post_listen_id' => $model->id));
              $video_thumb = VideoListens::model()->findByAttributes(array('post_listen_id'=> $model->id))->thumbnail;
              //$komen = count($model_advices);
          ?>
         


          <li class="media media-list-highlight">
          <a class="pull-left" href="#">
            <img class="media-object"src="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail_listens/<?php echo $video_thumb?>" alt="video thumbnail" width="144" height="96">
          </a>
          <div class="btn-group pull-right">
            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
              <small><?= Yii::t('labels', 'More');?> <span class="glyphicon glyphicon-collapse-down"></span></small>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li>
                <a href=""><small><?= Yii::t('labels', 'Mark as spam'); ?></small></a>
              </li>
            </ul>
          </div>
          <div class="media-body">
            <h4 class="media-heading">
              <a href="<?php echo Yii::app()->request->baseUrl?>/postListens/quizes?id=<?= $model->id ?>"><?= PostListens::getTitle($model->id) ?></a>
              <a href=""><small><?= PostListens::getLang($model->id); ?></small></a><br>
            </h4>
            <div class="media-subheading"><small><?= Yii::t('labels', 'by'); ?> <a href=""><?= User::getFullName($model->user_id) ?></a></small></div>
            <div class="media-content">content</div>
            <div class="media-footer">
              <a ><i class="fa fa-user"></i> <small><?= Quiz::getTakersCount($model->id) ?> <?= Yii::t('labels', 'took this quiz'); ?></small></a>
              <span class="tab"></span>
              <a ><i class="fa fa-thumbs-o-up"></i> <small><?= PostListens::getLike($model->id) ?> <?= Yii::t('labels', 'like'); ?></small></a>
              <span class="tab"></span>
              <a ><i class="fa fa-thumbs-o-down"></i> <small><?= PostListens::getDislike($model->id) ?> <?= Yii::t('labels', 'vote down'); ?></small></a>
              <span class="tab"></span>
              <span><small><?= PostListens::getDate($model->id) ?></small></span>
            </div>
          </div>
        </li>
          <?php
            }    
          ?>
        </ul>
      </div><!-- Correction pool -->
  </div><!-- Main content -->
</div><!-- Page content -->

<?php
  //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    var myPlayer = _V_("myVideo");

    function play($time) {
        // Do something when the event is fired
        myPlayer.currentTime($time); // 2 minutes into the video
        myPlayer.play();
    };
</script>