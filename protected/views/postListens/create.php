<?php
  /* @var $this PostController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Quiz Maker";
?>

<head>
    <script>
      var temp_upload;
    </script>
    <!--
      > Muaz Khan     - https://github.com/muaz-khan 
      > MIT License   - https://www.webrtc-experiment.com/licence/
      > Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
      -->
      <!-- script used for audio/video/gif recording -->
      <script src="https://www.webrtc-experiment.com/RecordRTC.js"> </script>
  </head>

<div class="row" id="pagetitle">
  <div class="col-lg-12">
    <h1><?= Yii::t('labels', 'Listening Tutorial Topic'); ?></h1>
  </div>
</div><!-- pagetitle -->
<div class="row" id="pagesubtitle">
  <div class="col-lg-12">
    <h2>Step 2: Record the Story in Your Choosen Language</h2>
  </div>
</div><!-- pagesubtitle -->
<div class="row" id="pagecontent">
  <div class="col-lg-6 space-top">
    <?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Tech Card
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
              <p>Story :</p>  
              <?= $stories; ?>
            </div>
          </div>
        </div>
      </div>
    <?php $this->endWidget(); ?>  
  </div>
  <div class="col-lg-6 space-top">
    <section class="experiment">      
      <video id="preview" text-align="center" controls style="border: 2px solid rgb(15, 158, 238); height: 260px; width: 100%;"></video>
      <button class="btn btn-default btn-small" id="record">  <span class="glyphicon glyphicon-record"></span> Record</button>
      <button class="btn btn-default btn-small" id="stop" disabled> <span class="glyphicon glyphicon-stop"> Stop</button>
      <button class="btn btn-default btn-small" id="delete" disabled> <span class="glyphicon glyphicon-trash"> Delete </button>

      <div id="container" style="padding:1em 1em;"></div>
    </section>
  </div>
  <div class="col-lg-12">
    <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
          'id' => 'post-form',
          'enableAjaxValidation' => false,
          'type' => 'horizontal',
          'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
          ),
        ));
      ?>
      <fieldset>
      <?php   
      // Title
        echo $form->textFieldGroup(
          $model_record,
          'title',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-md-8',
            ),
            'id' => "video-title",
            'maxlength' => 300,
            'labelOptions' => array(
              'label' => Yii::t('labels', 'Title')
            )
          )
        ); // Title

        // Speech language
        echo $form->dropDownListGroup(
          $model_record,
          'lang_id',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-md-8',
            ),
            'widgetOptions' => array(
              'data' => CHtml::listData($model_lang->findAll(), 'id', 'name'),
              'htmlOptions' => array(),
            )
          )
        ); // Speech language
        ?>
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <?php echo $form->hiddenField($model_record, 'upload_file', array('id' => 'upload_filename')); ?>
          <?php echo $form->error($model_record, 'upload_file'); ?>
        </div>
      </div>
      </fieldset>

      <div class="form-group">
        <div class="text-center">
          <?php
            $this->widget('booster.widgets.TbButton', array(
              'buttonType' => 'submit',
              'context' => 'primary',
              'label' => 'Post',
              'id' => "save",
              'htmlOptions' => array('disabled'=>'true'),
            ));
          ?>
        </div>
      </div>

      <?php
        $this->endWidget();
      ?>
  </div>
</div><!-- pagecontent -->

<script>
    // todo: this demo need to be updated for Firefox.
    // it currently focuses only chrome.
    
    function PostBlob(audioBlob, videoBlob, fileName) {
        var formData = new FormData();
        formData.append('filename', fileName);
        formData.append('audio-blob', audioBlob);
        formData.append('video-blob', videoBlob);
        xhr(<?= "'".Yii::app()->createUrl('site/save')."'" ?>, formData, function(ffmpeg_output) {
           // document.querySelector('h1').innerHTML = ffmpeg_output.replace( /\\n/g , '<br />');
            preview.src = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/uploads/temp/' + fileName + '-merged.webm';
            saveUpload.disabled=false;
            deleteFiles.disabled = false;
            preview.play();
            preview.muted = false;
        });
    }

    var record = document.getElementById('record');
    var stop = document.getElementById('stop');
    var deleteFiles = document.getElementById('delete');
    var saveUpload = document.getElementById('save');

    var audio = document.querySelector('audio');

    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');

    var container = document.getElementById('container');

    var isFirefox = !!navigator.mozGetUserMedia;

    var recordAudio, recordVideo;
    record.onclick = function() {
        record.disabled = true;
        !window.stream && navigator.getUserMedia({
                audio: true,
                video: true
            }, function(stream) {
                window.stream = stream;
                onstream();
            }, function(error) {
                alert(JSON.stringify(error, null, '\t'));
            });

        window.stream && onstream();

        
    };

    function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = true;

            recordAudio = RecordRTC(stream, {
                // bufferSize: 16384,
                onAudioProcessStarted: function() {
                    if(!isFirefox) {
                        recordVideo.startRecording();
                    }
                }
            });
            
            recordVideo = RecordRTC(stream, {
                type: 'video'
            });
            
            recordAudio.startRecording();

            stop.disabled = false;
        }

    var fileName;
    stop.onclick = function() {
       /// document.querySelector('h1').innerHTML = 'Getting Blobs...';

        record.disabled = false;
        stop.disabled = true;

        preview.src = '';
        preview.poster = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/images/' +'ajax-loader.gif';

        fileName = Math.round(Math.random() * 99999999) + 99999999;
        

        if (!isFirefox) {
            recordAudio.stopRecording(function() {
              //  document.querySelector('h1').innerHTML = 'Got audio-blob. Getting video-blob...';
                recordVideo.stopRecording(function() {
                   // document.querySelector('h1').innerHTML = 'Uploading to server...';
                    PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
                });
            });
        }
        
    };

    saveUpload.onclick = function() {
        document.getElementById('upload_filename').value=fileName + '-merged.webm';
    };

    deleteFiles.onclick = function() {
        deleteAudioVideoFiles();
    };

    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(data);
    }

    function deleteAudioVideoFiles() {
        deleteFiles.disabled = true;
         saveUpload.disabled=true;
        if (!fileName) return;
        var formData = new FormData();
        formData.append('delete-file', fileName);
        xhr(<?= "'".Yii::app()->createUrl('site/delete')."'" ?>, formData, null, null, function(response) {
            console.log(response);
        });
        fileName = null;
        container.innerHTML = '';
        preview.poster = '';
        preview.src='';
    }

</script>



