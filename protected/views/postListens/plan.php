<?php
/* @var $this PostController */

$this->pageTitle = "Submission";
?>

<!-- Page title -->
<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <h1>
      <small><?= Yii::t('labels', 'Plan Your Video Techcard') ?></small>
    </h1>
  </div>
</div>

<!-- Gap -->
<div class="row">
  <div class="col-md-12"></div>
</div>

<!-- Page content -->
<div class="row">
  <!-- Submenu -->
  <div class="col-md-2 text-right">
    <ul class="nav nav-pills nav-stacked">
      <li class="active" id="submenu-new">
        <a href="#">New</a>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-drafts">
        <a data-toggle="collapse" href="#collapse-draft" id="nav-drop-drafts">
          Drafts <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-drafts"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-draft">
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #3</a>
            </div>
          </li>
          <li class="media list-group-item text-right">
            <a href="#">See more</a>
          </li>
        </ul>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-recents">
        <a data-toggle="collapse" href="#collapse-recent" id="nav-drop-recents">
          Recents <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-recents"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-recent">
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #3</a>
            </div>
          </li>
          <li class="media list-group-item text-right">
            <a href="#">See more</a>
          </li>
        </ul>
      </li>
    </ul>
  </div><!-- Submenu -->

  <!-- Main content -->

  <div class="col-md-8">   

    <form class="form-horizontal" style="text-align:center">
       <div class="form-group">
          <div class="controls form-inline">
           <label> Theme </label> 
           <input type="text" class="form-control" value="<?php echo ''.$themename.''; ?>" disabled>
        </div>
      </div>

       <div class="form-group">
          <div class="controls form-inline">
           <label> Topic </label> 
           <input type="text" class="form-control" value="<?php echo ''.$topicname.''; ?>" disabled>
         </div>
      </div>
    </form>

      <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
          'id' => 'listen-post-form',
          'enableAjaxValidation' => false,
          'type' => 'horizontal',
          'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
          ),
        ));
      ?>
      <fieldset>

        <?php
          // Title
          echo $form->textFieldGroup(
            $model,
            'title',
            array(
              'wrapperHtmlOptions' => array(
                'class' => 'col-md-8',
              ),
              'id' => "video-title",
              'maxlength' => 300,
              'labelOptions' => array(
                'label' => Yii::t('labels', 'Title')
              )
            )
          ); // Title

          // Speech language
          echo $form->dropDownListGroup(
            $model,
            'lang_id',
            array(
              'wrapperHtmlOptions' => array(
                'class' => 'col-md-8',
              ),
              'widgetOptions' => array(
                'data' => CHtml::listData($model_lang->findAll(), 'id', 'name'),
                'htmlOptions' => array(),
              )
            )
          ); // Speech language

          //Questions
          echo $form->checkboxListGroup(
            $model,
            'detail',
            array(
              'wrapperHtmlOptions' => array(
                'class' => 'col-md-8',
                'disabled'=>'true',
              ),
              'widgetOptions' => array(
               'data' => CHtml::listData($topic_details->findAll('topic_id = :TOPIC', array(':TOPIC' => $topic)), 'id', 'description'),
              ),
            )
          );
        ?>
        
        
      <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
       <?php $this->widget(
          'booster.widgets.TbButton',
          array(
              'label' => '+ Custom Story Point!',
              'size' => 'small',
              'context' => 'success',
              'htmlOptions' => array(
                  'data-toggle' => 'modal',
                  'data-target' => '#addStoryModal',
                  
              ),
          )
      ); ?>
      </div>
    </div>
      <br>
      </fieldset>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <?php
            $this->widget('booster.widgets.TbButton', array(
              'buttonType' => 'submit',
              'context' => 'primary',
              'label' => 'Save Techcard',
            ));
          ?>
        </div>
      </div>

      <?php
        $this->endWidget();
      ?>
  </div><!-- Main content -->

  <!-- Zamrud point -->
  <div class="col-md-2">
    <div class="panel panel-primary">
      <div class="panel-heading text-center">
        <strong>Zamrud Points</strong>
      </div>
      <ul class="list-group">
        <li class="list-group-item">
          <p class="lead text-right no-space">
            5,325 <img src="<?php echo Yii::app()->baseUrl;?>/res/img/Green Emerald.png" width="24" />
          </p>
        </li>
        <li class="list-group-item">
          <p class="lead text-right no-space">
            925 <img src="<?php echo Yii::app()->baseUrl;?>/res/img/Red Emerald.png" width="24" />
          </p>
        </li>
      </ul>
    </div>
  </div><!-- Zamrud point -->
</div>

<?php $this->beginWidget(
    'booster.widgets.TbModal',
    array('id' => 'addStoryModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Add Story Point !</h4>
        <p> Please Add Story Point Based on Choosen Topic</p>
    </div>
 
    <div class="modal-body">
        <br>
        <?php
          $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'add-story-form',
            'enableAjaxValidation' => false,
            'type' => 'horizontal',
            'htmlOptions' => array(
              'enctype' => 'multipart/form-data'
            ),
          ));
        ?>
        <fieldset>
          <?php
            // Description
            echo $form->textFieldGroup(
              $model_story,
              'description',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "description",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Description')
                )
              )
            ); // Description

            // Question
            echo $form->textFieldGroup(
              $model_story,
              'question',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "question",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Question')
                )
              )
            ); // Question

            // Answer1
            echo $form->textFieldGroup(
              $model_story,
              'answer1',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "answer1",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Answer 1')
                )
              )
            ); // Answer1


            // Answer2
            echo $form->textFieldGroup(
              $model_story,
              'answer2',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "answer2",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Answer 2')
                )
              )
            ); // Answer2

            // Answer3
            echo $form->textFieldGroup(
              $model_story,
              'answer3',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "answer3",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Answer 3')
                )
              )
            ); // Answer3

            // Answer4
            echo $form->textFieldGroup(
              $model_story,
              'answer4',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'id' => "answer4",
                'maxlength' => 300,
                'labelOptions' => array(
                  'label' => Yii::t('labels', 'Answer 4')
                )
              )
            ); // Answer4


            // Answer True
            echo $form->dropDownListGroup(
              $model_story,
              'answer_true',
              array(
                'wrapperHtmlOptions' => array(
                  'class' => 'col-md-8',
                ),
                'widgetOptions' => array(
                  'data' => array('1'=>1,'2'=>2,'3'=>3,'4'=>4),
                  'htmlOptions' => array(),
                )
              )
            ); // Answer True

            ?>
        </fieldset>   
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'context' => 'primary',
                'label' => 'Add',
                //'buttonType' => 'submit',
                'url' => $this->createUrl('postlistens/story'),
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>

        <?php
          $this->endWidget();
        ?>

        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>