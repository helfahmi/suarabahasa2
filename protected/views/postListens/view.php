<?php
  /* @var $this SiteController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Video Watch";
?>

<head>
  <link href="http://vjs.zencdn.net/4.6/video-js.css" rel="stylesheet"> </link>
    <script src="http://vjs.zencdn.net/4.6/video.js"></script>
    <style type="text/css">
      .vjs-default-skin { color: #1b71a4; }
      .vjs-default-skin .vjs-play-progress,
      .vjs-default-skin .vjs-volume-level { background-color: #070808 }
      .vjs-default-skin .vjs-control-bar,
      .vjs-default-skin .vjs-big-play-button { background: rgba(232,226,226,0.7) }
      .vjs-default-skin .vjs-slider { background: rgba(232,226,226,0.2333333333333333) }
      .vjs-default-skin .vjs-control-bar { font-size: 127% }
    </style>
</head>

<div class="row" id="pagetitle">
  <div class="col-lg-12">
    <h2><?= PostListens::getTitle($model_post_listen->id) ?><a href=""><small> <?= PostListens::getLang($model_post_listen->id) ?></small></a></h2>
  </div>
</div><!-- pagetitle -->
<div class="row" id="pagecontent">
  <!-- Main content -->
  <div class="col-lg-12">
    <div class="panel panel-default">
      <!-- Video container -->
      <div class="inner-panel">
        <!-- Video -->
        <video id="myVideo" class="video-js vjs-default-skin box" controls preload="auto" width="100%" height="480" poster="<?= Yii::app()->baseUrl . '/uploads/thumbnail_listens/' . VideoListens::getThumbnail($model_video_listen->id) ?>" data-setup="{}">
          <source src="<?= Yii::app()->baseUrl . '/uploads/video_listens/' .VideoListens::getVideo($model_post_listen->id) ?>" type='video/mp4' />
          <source src="<?= Yii::app()->baseUrl . '/uploads/video_listens/' .VideoListens::getVideo($model_post_listen->id) ?>" type='video/webm' />
        </video><!-- Video -->

        <!-- Statistics -->
        <button type="button" class="btn btn-link pull-right" disabled>
          <span class="glyphicon glyphicon-thumbs-up"></span> <?= PostListens::getLike($model_post_listen->id) ?> Like
        </button>
        <!--
        <button type="button" class="btn btn-link pull-right">
          <span class="glyphicon glyphicon-comment"></span> 4 Comment
        </button>
      -->
        <!-- Statistics -->

        <!-- Gap -->
        <div class="row">
          <div class="col-md-12"></div>
        </div><!-- Gap -->

        <!-- Creator and video's detail -->
        <div class="media">
          <a class="pull-left" href="#">
            <img class="media-object" src="<?= Yii::app()->baseUrl . '/uploads/account/' ?><?= User::getPicture(Yii::app()->user->getId()) != '' ? User::getPicture(Yii::app()->user->getId()) : 'defaultpp.png' ?>" width="52" height="52">
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?= PostListens::getUser($model_post_listen->id) ?><small> <?= $model_post_listen->date ?>  </small></h4>
            <h4>
              <small>
                <span class="glyphicon glyphicon-home"></span>
              </small>
              <a href=""><small> Dalam <?= Lang::getLangName($model_post_listen->lang_id) ?></small></a>
              <!-- <a href=""><small>English</small></a> -->
            </h4>
            <button id="play_1" type="button" class="btn btn-default btn-sm" onClick="play(0)">
              <span class="glyphicon glyphicon-play"></span> 1
            </button><br><br>
            <p>Quiz tentang <?= Themes::getName($model_post_listen->theme_id) ?></p>
            <p>

              <a class="btn btn-primary btn-md" onClick="showTests()">Take the test!</a>
              <div class="tests" style="display:none">
                <?php /** @var TbActiveForm $form */
                  $form = $this->beginWidget(
                    'booster.widgets.TbActiveForm',
                    array(
                      'id' => 'horizontalForm',
                      'type' => 'horizontal',
                    )
                  ); ?>
                    <fieldset>
                    
                      <legend>Questions</legend>
                      <?php for($i=0;$i<count($questions);$i++){
                         $q = $questions[$i]; 
                         echo ($i + 1) . ". " .$q->question . "<br>";
                         echo $form->radioButtonListGroup($answer, 'answer'. ($i + 1),
                          array(
                            'labelOptions' => array(
                              //'label' => false
                             ),
                            'widgetOptions' => array(
                              'data' => array(
                                  $q->answer1,
                                  $q->answer2,
                                  $q->answer3,
                                  $q->answer4
                                )
                            )
                          )
                        );
                      } ?>
                    </fieldset>
                  <div class="form-actions">
                    <?php $this->widget(
                      'booster.widgets.TbButton',
                      array(
                        'buttonType' => 'submit',
                        'context' => 'primary',
                        'label' => 'Submit'
                      )
                    ); ?>
                    <?php $this->widget(
                      'booster.widgets.TbButton',
                      array('buttonType' => 'reset', 'label' => 'Reset')
                    ); ?>
                  </div>

                  <?php $this->endWidget() ?>
              </div>
            </p>

            &nbsp; &nbsp;
            
          </div>
        </div><!-- Creator and video's detail -->
      </div><!-- Video container -->

      <hr>

      <!-- Correction pool -->
      <div class="inner-panel">
        <h4>Quiz History</h4>
        <ul class="media-list panel-comment">

        <li class="media list-highlight">
            <!-- Avatar -->
            <a class="pull-left" href="#">
              <img class="media-object" src="<?= Yii::app()->baseUrl . '/uploads/account/' ?><?= User::getPicture(Yii::app()->user->getId()) != '' ? User::getPicture(Yii::app()->user->getId()) : 'defaultpp.png' ?>" width="48" height="48">
            </a><!-- Avatar -->
            <!-- More option -->
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                <small>More <span class="glyphicon glyphicon-collapse-down"></span></small>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="">Mark as spam</a>
                </li>
              </ul>
            </div><!-- More option -->  
            <!-- Content -->
            <div class="media-body">
              <h5 class="media-heading">
                <b>Harits Elfahmi</b>
                <small>
                  <span class="glyphicon glyphicon-home"></span>
                </small>
                <a href=""><small>Bahasa Indonesia</small></a>
                <span class="tab-small"></span>
                <small>
                  <span class="glyphicon glyphicon-screenshot"></span>
                </small>
                <a href=""><small>Sundanese</small></a>
              </h5>
              Scored 50 of 100<br>
            </div><!-- Content -->
          </li>

<!-- 1 COMMENT END --> 

          <li class="media list-highlight text-center">
            <button type="button" class="btn btn-link">See More</button>
          </li>
        </ul>
      </div><!-- Correction pool -->
    </div>
  </div><!-- Main content -->
</div><!-- pagecontent -->

<?php
  //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    var myPlayer = _V_("myVideo");

    function play($time) {
        // Do something when the event is fired
        myPlayer.currentTime($time); // 2 minutes into the video
        myPlayer.play();
    };  

    function showTests(){
      $(".tests").fadeIn(300);
    }
</script>