<?php
/* @var $this PostController */

$this->pageTitle = "Submission";
?>

<!-- Page title -->
<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <h1>
      <small><?= Yii::t('labels', 'Speaking Video Post') ?></small>
    </h1>
  </div>
</div>

<!-- Gap -->
<div class="row">
  <div class="col-md-12"></div>
</div>

<!-- Page content -->
<div class="row">
  <!-- Submenu -->
  <div class="col-md-2 text-right">
    <ul class="nav nav-pills nav-stacked">
      <li class="active" id="submenu-new">
        <a href="#">New</a>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-drafts">
        <a data-toggle="collapse" href="#collapse-draft" id="nav-drop-drafts">
          Drafts <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-drafts"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-draft">
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Draft #3</a>
            </div>
          </li>
          <li class="media list-group-item text-right">
            <a href="#">See more</a>
          </li>
        </ul>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-recents">
        <a data-toggle="collapse" href="#collapse-recent" id="nav-drop-recents">
          Recents <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-recents"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-recent">
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <a class="pull-left" href="#">
              <img class="media-object" src="..."/>
            </a>
            <div class="media-body">
              <a href="#">Recent #3</a>
            </div>
          </li>
          <li class="media list-group-item text-right">
            <a href="#">See more</a>
          </li>
        </ul>
      </li>
    </ul>
  </div><!-- Submenu -->

  <!-- Main content -->

  <head>
    <script>
      var temp_upload;
    </script>
    <!--
      > Muaz Khan     - https://github.com/muaz-khan 
      > MIT License   - https://www.webrtc-experiment.com/licence/
      > Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
      -->
      <!-- script used for audio/video/gif recording -->
      <script src="https://www.webrtc-experiment.com/RecordRTC.js"> </script>
  </head>

  <div class="col-md-8">

    <?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
      <div class="panel-group" id="accordion">
        <div class="col-md-5">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Tech Card : <?php echo ''.$title.''; ?>
              </a>
            </h4>
          </div>
          <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
              <p>Theme : <?php echo ''.$_GET['themename'].''; ?> </p>
              <p>Topic : <?php echo ''.$_GET['topicname'].''; ?> </p>
              <p>Story Point :</p>
              <ul>
                <?php
                foreach($models as $model){
                  ?>
                    <li><?= TopicDetails::getDescription($model->topic_detail_id) ?></li>
                  <?php
                }    
            ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php $this->endWidget(); ?>

    <div class="col-md-7">
       <div style="text-align:center" >
          <section class="experiment">  

            <p style="text-align:center;">
              <video id="preview" controls style="border: 2px solid rgb(15, 158, 238); height: 260px; width: 390px;"></video> 
            </p>
            <hr />

            <button class="btn btn-default btn-small" id="record">  <span class="glyphicon glyphicon-record"></span> Record</button>
            <button class="btn btn-default btn-small" id="stop" disabled> <span class="glyphicon glyphicon-stop"> Stop</button>
            <button class="btn btn-default btn-small" id="delete" disabled> <span class="glyphicon glyphicon-trash"> Delete </button>

            <div id="container" style="padding:1em 1em;"></div>
          </section>               
      </div>

      <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
          'id' => 'post-form',
          'enableAjaxValidation' => false,
          'type' => 'horizontal',
          'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
          ),
        ));
      ?>
      <fieldset>
        
      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?php echo $form->hiddenField($model_record, 'upload_file', array('id' => 'upload_filename')); ?>
            <?php echo $form->error($model_record, 'upload_file'); ?>
          </div>
        </div>
      </fieldset>

      <div class="form-group">
        <div style="text-align:center">
          <?php
            $this->widget('booster.widgets.TbButton', array(
              'buttonType' => 'submit',
              'context' => 'primary',
              'label' => 'Post',
              'id' => "save",
              'htmlOptions' => array('disabled'=>'true'),
            ));
          ?>
        </div>
      </div>

      <?php
        $this->endWidget();
      ?>
    </div>
  </div><!-- Main content -->

<?php
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script>
    // todo: this demo need to be updated for Firefox.
    // it currently focuses only chrome.
    
    function PostBlob(audioBlob, videoBlob, fileName) {
        var formData = new FormData();
        formData.append('filename', fileName);
        formData.append('audio-blob', audioBlob);
        formData.append('video-blob', videoBlob);
        xhr(<?= "'".Yii::app()->createUrl('site/save')."'" ?>, formData, function(ffmpeg_output) {
           // document.querySelector('h1').innerHTML = ffmpeg_output.replace( /\\n/g , '<br />');
            preview.src = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/uploads/temp/' + fileName + '-merged.webm';
            saveUpload.disabled=false;
            deleteFiles.disabled = false;
            preview.play();
            preview.muted = false;
        });
    }

    var record = document.getElementById('record');
    var stop = document.getElementById('stop');
    var deleteFiles = document.getElementById('delete');
    var saveUpload = document.getElementById('save');

    var audio = document.querySelector('audio');

    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');

    var container = document.getElementById('container');

    var isFirefox = !!navigator.mozGetUserMedia;

    var recordAudio, recordVideo;
    record.onclick = function() {
        record.disabled = true;
        !window.stream && navigator.getUserMedia({
                audio: true,
                video: true
            }, function(stream) {
                window.stream = stream;
                onstream();
            }, function(error) {
                alert(JSON.stringify(error, null, '\t'));
            });

        window.stream && onstream();

        
    };

    function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = true;

            recordAudio = RecordRTC(stream, {
                // bufferSize: 16384,
                onAudioProcessStarted: function() {
                    if(!isFirefox) {
                        recordVideo.startRecording();
                    }
                }
            });
            
            recordVideo = RecordRTC(stream, {
                type: 'video'
            });
            
            recordAudio.startRecording();

            stop.disabled = false;
        }

    var fileName;
    stop.onclick = function() {
       /// document.querySelector('h1').innerHTML = 'Getting Blobs...';

        record.disabled = false;
        stop.disabled = true;

        preview.src = '';
        preview.poster = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/images/' +'ajax-loader.gif';

        fileName = Math.round(Math.random() * 99999999) + 99999999;
        

        if (!isFirefox) {
            recordAudio.stopRecording(function() {
              //  document.querySelector('h1').innerHTML = 'Got audio-blob. Getting video-blob...';
                recordVideo.stopRecording(function() {
                   // document.querySelector('h1').innerHTML = 'Uploading to server...';
                    PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
                });
            });
        }
        
    };

    saveUpload.onclick = function() {
        document.getElementById('upload_filename').value=fileName + '-merged.webm';
    };

    deleteFiles.onclick = function() {
        deleteAudioVideoFiles();
    };

    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(data);
    }

    function deleteAudioVideoFiles() {
        deleteFiles.disabled = true;
         saveUpload.disabled=true;
        if (!fileName) return;
        var formData = new FormData();
        formData.append('delete-file', fileName);
        xhr(<?= "'".Yii::app()->createUrl('site/delete')."'" ?>, formData, null, null, function(response) {
            console.log(response);
        });
        fileName = null;
        container.innerHTML = '';
        preview.poster = '';
        preview.src='';
    }

</script>
