<?php
  /* @var $this SiteController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Video Watch";
?>

<div class="row" id="pagetitle">
  <div class="col-lg-12">
    <h2><?= PostListens::getTitle($model_post_listen->id) ?><a href=""><small> <?= PostListens::getLang($model_post_listen->id) ?></small></a></h2>
  </div>
</div><!-- pagetitle -->
<div class="row" id="pagecontent">
  <!-- Main content -->
  <div class="col-lg-12">
    <div class="panel panel-default">
      <!-- Video container -->
      <div class="inner-panel text-center">

        <h1>You get score <?= $score ?>!</h1>

        <a class="btn btn-primary btn-md" href="<?= Yii::app()->createUrl('postListens/quizes?id='. $model_post_listen->id) ?>">Go Back</a>

      </div><!-- Correction pool -->
    </div>
  </div><!-- Main content -->
</div><!-- pagecontent -->

<?php
  //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    var myPlayer = _V_("myVideo");

    function play($time) {
        // Do something when the event is fired
        myPlayer.currentTime($time); // 2 minutes into the video
        myPlayer.play();
    };  

    function showTests(){
      $(".tests").fadeIn(300);
    }
</script>