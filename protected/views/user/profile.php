<?php
	/* @var $this SiteController */
	$this->pageTitle=Yii::app()->name." - Profile";
?>

<div class="col-lg-2">
	<div class="row" id="profilepicture">
		<div class="col-lg-12">
			<img class="img-responsive" src="<?= Yii::app()->request->baseUrl?>/uploads/account/<?= User::getPicture($model->id) != '' ? User::getPicture($model->id) : 'defaultpp.png' ?>" alt="Profile Picture" width="128">
			<button class="btn btn-link"><?= Yii::t('labels', 'Change picture'); ?></button>
		</div>
	</div><!-- profilepicture -->
</div>
<div class="col-lg-10">
	<div class="row" id="pagetitle">
		<div class="col-lg-12">
			<h1><?= User::getFullName($model->id) ?> <a href="#"><small><i class="fa fa-pencil"></i> edit</small></a></h1>
		</div>
	</div><!-- pagetitle -->
	<div class="row" id="pagecontent">
		<div class="col-lg-12">
			<div class="row" id="userabout">
				<div class="col-lg-6">
					<h3><?= Yii::t('labels', 'Profile'); ?> <i class="fa fa-male"></i></h3>
					<i class="fa fa-home fa-fw"></i> <b><?= Yii::t('labels', 'Native language'); ?></b><br>
					<i class="fa fa-fw"></i> <?= User::getNativeLang($model->id) ?><br>
					<i class="fa fa-bullseye fa-fw"></i> <b><?= Yii::t('labels', 'Target language'); ?></b><br>
					<i class="fa fa-fw"></i> <?= User::getLearnLang($model->id) ?><br>
					<i class="fa fa-calendar-o fa-fw"></i> <b><?= Yii::t('labels', 'Birth'); ?></b><br>
					<i class="fa fa-fw"></i> <?= User::getTtl($model->id) ?><br>
					<i class="fa fa-gamepad fa-fw"></i> <b><?= Yii::t('labels', 'Hobby'); ?></b><br>
					<i class="fa fa-fw"></i> [Di DB Belum ada Hobi]<br>
					<i class="fa fa-user fa-fw"></i> <b><?= Yii::t('labels', 'About'); ?></b><br>
					<i class="fa fa-fw"></i> [Di DB belum ada about]
				</div>
				<div class="col-lg-6">
					<h3>Statistics</h3>
				</div>
			</div>
			<div class="row" id="uservideo">
				<div class="col-lg-12">
					<h3><?= Yii::t('labels', 'My Speaking Practice Video'); ?> <small><a href=""><?= Yii::t('labels', 'see all'); ?></a></small></h3>
					<?php
			            foreach($modelposts as $modelpost) {
			               $video_thumb = Videos::model()->findByAttributes(array('post_id'=> $modelpost->id))->thumbnail;
			               $video_title = Posts::model()->findByAttributes(array('id'=> $modelpost->id))->title;
			          ?>
					<div class="col-lg-3 text-center">
						<a href="#" class="thumbnail">
							<img src="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail/<?php echo $video_thumb?>" alt="video thumbnail" width="144" height="96" alt="video thumbnail">
							<h5><?php echo $video_title ?></h5>
						</a>
					</div>
					<?php
						}
					?>
					
				</div>
			</div>
			<div class="row" id="useradvice">
				<div class="col-lg-12">
					<h3><?= Yii::t('labels', 'My Listening Help Other Video'); ?> <small><a href=""><?= Yii::t('labels', 'see all'); ?></a></small></h3>
					<?php
			            foreach($modelpostlistens as $modelpostlisten) {
			               $video_thumb = VideoListens::model()->findByAttributes(array('post_listen_id'=> $modelpostlisten->id))->thumbnail;
			               $video_title = PostListens::model()->findByAttributes(array('id'=> $modelpostlisten->id))->title;
			          ?>
					<div class="col-lg-3 text-center">
						<a href="#" class="thumbnail">
							<img src="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail_listens/<?php echo $video_thumb?>" alt="video thumbnail" width="144" height="96" alt="video thumbnail">
							<h5><?php echo $video_title?></h5>
						</a>
					</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div><!-- pagecontent -->
</div>
