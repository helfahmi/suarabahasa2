<?php
	/* @var $this SiteController */
	$this->pageTitle=Yii::app()->name." - Fame";
?>

<h1>Fame</h1>
<hr>
<div>
	<div class="col-lg-1 text-right"><h2>1</h2></div>
	<div class="col-lg-11 panel panel-default">
		<div class="panel-body">
			<div class="pull-left">
				<h3 class="no-space"><img src="<?= Yii::app()->baseUrl;?>/uploads/account/harits.jpg" class="" width="48" height="48"> <a href="">Muhammad Harits Elfahmi</a></h3>
			</div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/researcher.png" height="24" /><br>
				<span><b>300</b></span>
			</div>
			<div class="pull-right space-hor-normal"></div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/master.png" height="24" /><br>
				<span><b>600</b></span>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="col-lg-1 text-right"><h2>2</h2></div>
	<div class="col-lg-11 panel panel-default">
		<div class="panel-body">
			<div class="pull-left">
				<h3 class="no-space"><img src="<?= Yii::app()->baseUrl;?>/uploads/account/nashir.jpg" class="" width="48" height="48"> <a href="">Muhammad Nashiruddin</a></h3>
			</div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/researcher.png" height="24" /><br>
				<span><b>275</b></span>
			</div>
			<div class="pull-right space-hor-normal"></div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/professor.png" height="24" /><br>
				<span><b>460</b></span>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="col-lg-1 text-right"><h2>3</h2></div>
	<div class="col-lg-11 panel panel-default">
		<div class="panel-body">
			<div class="pull-left">
				<h3 class="no-space"><img src="<?= Yii::app()->baseUrl;?>/uploads/account/alifanuraniputri.jpg" class="" width="48" height="48"> <a href="">Alifa Nurani Putri</a></h3>
			</div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/student.png" height="24" /><br>
				<span><b>98</b></span>
			</div>
			<div class="pull-right space-hor-normal"></div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/professor.png" height="24" /><br>
				<span><b>435</b></span>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="col-lg-1 text-right"><h2>4</h2></div>
	<div class="col-lg-11 panel panel-default">
		<div class="panel-body">
			<div class="pull-left">
				<h3 class="no-space"><img src="<?= Yii::app()->baseUrl;?>/uploads/account/defaultpp.png" class="" width="48" height="48"> <a href="">Jais Anasrullah</a></h3>
			</div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/schooler.png" height="24" /><br>
				<span><b>48</b></span>
			</div>
			<div class="pull-right space-hor-normal"></div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/assistant.png" height="24" /><br>
				<span><b>40</b></span>
			</div>
		</div>
	</div>
</div>
<div>
	<div class="col-lg-1 text-right"><h2>5</h2></div>
	<div class="col-lg-11 panel panel-default">
		<div class="panel-body">
			<div class="pull-left">
				<h3 class="no-space"><img src="<?= Yii::app()->baseUrl;?>/uploads/account/defaultpp.png" class="" width="48" height="48"> <a href="">Innani Yudho'afi</a></h3>
			</div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/tk.png" height="24" /><br>
				<span><b>24</b></span>
			</div>
			<div class="pull-right space-hor-normal"></div>
			<div class="pull-right text-center">
				<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/teacher.png" height="24" /><br>
				<span><b>10</b></span>
			</div>
		</div>
	</div>
</div>