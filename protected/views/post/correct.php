<?php
  /* @var $this SiteController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Video Watch";
?>

<head>
  <link href="http://vjs.zencdn.net/4.6/video-js.css" rel="stylesheet"> </link>
    <script src="http://vjs.zencdn.net/4.6/video.js"></script>
    <style type="text/css">
      .vjs-default-skin { color: #1b71a4; }
      .vjs-default-skin .vjs-play-progress,
      .vjs-default-skin .vjs-volume-level { background-color: #070808 }
      .vjs-default-skin .vjs-control-bar,
      .vjs-default-skin .vjs-big-play-button { background: rgba(232,226,226,0.7) }
      .vjs-default-skin .vjs-slider { background: rgba(232,226,226,0.2333333333333333) }
      .vjs-default-skin .vjs-control-bar { font-size: 127% }
    </style>
</head>

<div class="row" id="pagetitle">
  <div class="col-lg-12">
    <h2><?= Posts::getTitle($model_post->id) ?> <a href=""><small> <?= Posts::getLang($model_post->id) ?></small></a></h2>
  </div>
</div><!-- pagetitle -->
<div class="row" id="pagecontent">
  <!-- Main content -->
  <div class="col-lg-12">
    <div class="panel panel-default">
      <!-- Video container -->
      <div class="inner-panel">
        <!-- Video -->
        <video id="myVideo" class="video-js vjs-default-skin box" controls preload="auto" width="100%" height="480" poster="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail/<?= Videos::model()->findByPk($model_video->id)->thumbnail ?>" data-setup="{}">
          <source src="<?php echo Yii::app()->request->baseUrl?>/uploads/videos/<?= Videos::getURL($model_video->id) ?>" type='video/mp4' />
          <source src="<?php echo Yii::app()->request->baseUrl?>/uploads/videos/<?= Videos::getURL($model_video->id) ?>" type='video/webm' />
        </video><!-- Video -->

        <!-- Statistics -->
        <button type="button" class="btn btn-link pull-right" disabled>
          <span class="glyphicon glyphicon-thumbs-up"></span> <?= Posts::getLike($model_post->id) ?> Like
        </button>
        <button type="button" class="btn btn-link pull-right" disabled>
          <span class="glyphicon glyphicon-user"></span> <?= Posts::getWatch($model_post->id) ?> Watch
        </button>
        <!--
        <button type="button" class="btn btn-link pull-right">
          <span class="glyphicon glyphicon-comment"></span> 4 Comment
        </button>
      -->
        <button type="button" class="btn btn-link pull-right" disabled>
          <span class="glyphicon glyphicon-pencil"></span> <?= count($models); ?> Correction
        </button>
        <!-- Statistics -->

        <!-- Gap -->
        <div class="row">
          <div class="col-md-12"></div>
        </div><!-- Gap -->

        <!-- Creator and video's detail -->
        <div class="media">
          <a class="pull-left" href="#">
            <img class="media-object" src="<?php echo Yii::app()->request->baseUrl?>/uploads/account/<?= User::getPicture($model_post->user_id) ?>" width="52" height="52">
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?= Posts::getUser($model_post->id) ?> <small> <?= Posts::getDate($model_post->id) ?>  </small></h4>
            <h4>
              <small>
                <span class="glyphicon glyphicon-home"></span>
              </small>
              <a href=""><small><?= User::getNativeLang($model_post->user_id) ?></small></a>
              <span class="tab"></span>
              <small>
                <span class="glyphicon glyphicon-screenshot"></span>
              </small>
              <a href=""><small><?= User::getLearnLang($model_post->user_id) ?></small></a>
              <!-- <a href=""><small>English</small></a> -->
            </h4>
            <p><?= Posts::getContent($model_post->id) ?></p>
            <p></p>

            <?php
              $i = 0;
              foreach($model_tags as $model_tag){
                $i++;
                ?>
                <button id="play_1" type="button" class="btn btn-default btn-sm" onClick="play(<?= Tags::getTime($model_tag->id) ?>)">
                  <span class="glyphicon glyphicon-play"></span> <?= $i ?>
                </button>
                &nbsp; &nbsp;
                <?php
                }    
            ?>

          </div>
        </div><!-- Creator and video's detail -->
      </div><!-- Video container -->

      <hr>

      <!-- Correction form -->
      <div class="inner-panel">
        <!-- Audio correction -->
        <h4>Give A Correction</h4>
        
        <!-- Additional comment -->
        <div class="media">
          <a class="pull-left" href="#">
            <img class="media-object" src="<?php echo Yii::app()->request->baseUrl?>/uploads/account/<?= User::getPicture(Yii::app()->user->getId()) ?>" width="48" height="48">
          </a>
          <div class="media-body">
            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
              'id'=>'comment-form',
              //'enableAjaxValidation'=>true,
              'type'=>'horizontal',
              //'htmlOptions' => array('enctype' => 'multipart/form-data'),
            )); ?>
              
              <?php echo $form->errorSummary($model); ?>
              
              <?php echo $form->textAreaGroup($model,'content', array(
                  'wrapperHtmlOptions' => array(
                    'class' => 'col-md-12',
                    //'maxlength' => 45,
                  ),
                  'labelOptions' => array('label' => false),
                  'widgetOptions' => array(
                    //'labelOptions' => array('label' => false),
                    'htmlOptions' => array('rows' => 5),
                  ),
                  //'hint' => 'Maksimal 45 karakter.'
              )); ?>

              <div class="form-actions">
                <?php $this->widget('booster.widgets.TbButton', array(
                  'buttonType'=>'submit',
                  'context'=>'primary',
                  'label'=> 'Correct',
                )); ?>
              </div>

            <?php $this->endWidget(); ?>
          </div>
        </div>
        <!-- Additional comment -->
      </div>
      <!-- Correction form -->

      <hr>

      <!-- Correction pool -->
      <div class="inner-panel">
        <h4>Corrections</h4>
        <ul class="media-list panel-comment">

<!-- 1 COMMENT START --> 

  <?php
      foreach($models as $model){
        ?>

        <li class="media list-highlight">
            <!-- Avatar -->
            <a class="pull-left" href="#">
              <img class="media-object" src="<?php echo Yii::app()->request->baseUrl?>/uploads/account/<?= User::getPicture($model->user_id) ?>" width="48" height="48">
            </a><!-- Avatar -->
            <!-- More option -->
            <div class="btn-group pull-right">
              <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                <small>More <span class="glyphicon glyphicon-collapse-down"></span></small>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="">Mark as spam</a>
                </li>
              </ul>
            </div><!-- More option -->  
            <!-- Content -->
            <div class="media-body">
              <h5 class="media-heading">
                <b><?= User::getFullName($model->user_id) ?></b>
                <small>
                  <span class="glyphicon glyphicon-home"></span>
                </small>
                <a href=""><small><?= User::getNativeLang($model->user_id) ?></small></a>
                <span class="tab-small"></span>
                <small>
                  <span class="glyphicon glyphicon-screenshot"></span>
                </small>
                <a href=""><small><?= User::getLearnLang($model->user_id) ?></small></a>
              </h5>
              <?= $model->content ?><br>
              <button type="button" class="btn btn-link">
                <small>2 <span class="glyphicon glyphicon-thumbs-up"></span> Like</small>
              </button>
              <button type="button" class="btn btn-link">
                <small>0 <span class="glyphicon glyphicon-comment"></span> Comment</small>
              </button>
            </div><!-- Content -->
          </li>

        <?php
      }    
  ?>

<!-- 1 COMMENT END --> 
          <li class="media list-highlight text-center">
            <button type="button" class="btn btn-link">See More</button>
          </li>
        </ul>
      </div><!-- Correction pool -->
    </div>
  </div><!-- Main content -->
</div><!-- pagecontent -->

<?php
  //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    var myPlayer = _V_("myVideo");

    function play($time) {
        // Do something when the event is fired
        myPlayer.currentTime($time); // 2 minutes into the video
        myPlayer.play();
    };  
</script>