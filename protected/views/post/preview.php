<?php
/* @var $this SiteController */

$this->pageTitle = "Correction";
?>

<head>
  <link href="http://vjs.zencdn.net/4.6/video-js.css" rel="stylesheet"> </link>
    <script src="http://vjs.zencdn.net/4.6/video.js"></script>
    <style type="text/css">
      .vjs-default-skin { color: #1b71a4; }
      .vjs-default-skin .vjs-play-progress,
      .vjs-default-skin .vjs-volume-level { background-color: #070808 }
      .vjs-default-skin .vjs-control-bar,
      .vjs-default-skin .vjs-big-play-button { background: rgba(232,226,226,0.7) }
      .vjs-default-skin .vjs-slider { background: rgba(232,226,226,0.2333333333333333) }
      .vjs-default-skin .vjs-control-bar { font-size: 127% }
    </style>
    <script>
      var times = [];  times[0] = 0; 
      document.getElementById('tag_1').value = 0;
    </script>
</head>

<!-- Page title -->
<div class="row">
  <div class="col-md-offset-2 col-md-8">
    <h2>
      Tag Your Video! <a href=""><small>let's help other easy move to any sentence</small></a>
    </h2>
  </div>
</div><!-- Page title -->

<!-- Gap -->
<div class="row">
  <div class="col-md-12">
    <h1></h1>
  </div>
</div><!-- Gap -->

<!-- Page content -->
<div class="row">
  <!-- Submenu -->
  <div class="col-md-2 text-right"></div><!-- Submenu -->

  <!-- Main content -->
  <div class="col-md-8">
    <div class="panel panel-default">
      <!-- Video container -->
      <div class="inner-panel">
        <!-- Video -->

    
    <video id="myVideo" class="video-js vjs-default-skin box" controls preload="auto" width="588" height="392" poster="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail/<?php echo $video_thumb?>" type='video/mp4' data-setup="{}">
      <source src="<?php echo Yii::app()->request->baseUrl?>/uploads/videos/<?php echo $video_file?>" type='video/mp4' />
      <source src="<?php echo Yii::app()->request->baseUrl?>/uploads/videos/<?php echo $video_file?>" type='video/webm' />
    </video>
    <p> <p>

    <?php $this->widget(
        'booster.widgets.TbButton',
        array(
            'label' => '+ Tag',
            'context' => 'success',
            'htmlOptions'=>array(
              'onclick' => 'addTimes()',
            ),
         )
    ); ?>

    &nbsp; &nbsp;

    <?php $this->widget(
        'booster.widgets.TbButton',
        array(
            'label' => 'Reset Tags',
            'context' => 'danger',
            'htmlOptions'=>array(
              'onclick' => 'resetTags()',
            ),
         )
    ); ?>

    <br><br>
    <button id="play_1" type="button" class="btn btn-default" onClick="playTAG(0)">
      <span class="glyphicon glyphicon-play"></span> 1
    </button>

    &nbsp; &nbsp; &nbsp; &nbsp;
    
    <button id="play_2" type="button" class="btn btn-default" onClick="playTAG(1)" disabled>
      <span class="glyphicon glyphicon-play"></span> 2
    </button>

    <br><br>
    <button id="play_3" type="button" class="btn btn-default" onClick="playTAG(2)"  disabled>
      <span class="glyphicon glyphicon-play"></span> 3
    </button>

    &nbsp; &nbsp; &nbsp; &nbsp;

    <button id="play_4" type="button" class="btn btn-default" onClick="playTAG(3)"  disabled>
      <span class="glyphicon glyphicon-play"></span> 4
    </button>

    <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
          'id' => 'tag-form',
          'enableAjaxValidation' => false,
          'type' => 'horizontal',
          'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
          ),
        ));
      ?>
      <fieldset>
          <?php echo $form->hiddenField($model_tag, 'tag_1', array('id' => 'tag_1')); ?>
          <?php echo $form->hiddenField($model_tag, 'tag_2', array('id' => 'tag_2')); ?>
          <?php echo $form->hiddenField($model_tag, 'tag_3', array('id' => 'tag_3')); ?>
          <?php echo $form->hiddenField($model_tag, 'tag_4', array('id' => 'tag_4')); ?>
      </fieldset>

      <?php
        $this->widget(
            'booster.widgets.TbPanel',
            array(
                'title' => $title." (by ".Yii::app()->user->name.")",
            //'context' => 'primary',
               // 'headerIcon' => 'home',
                'content' => $content,
            )
        );
      ?>

      <?php
        $this->widget('booster.widgets.TbButton', array(
          'buttonType' => 'submit',
          'context' => 'primary',
          'label' => 'Post',
        ));
      ?>

      <?php
        $this->endWidget();
      ?>

   </div> <!-- Video -->
    </div>
    </div>
  </div><!-- Main content -->

  
</div><!-- Page content -->

<?php
  //Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script type="text/javascript">
    var myPlayer = _V_("myVideo");

    function play($time) {
        // Do something when the event is fired
        myPlayer.currentTime($time); // 2 minutes into the video
        myPlayer.play();
    };

    function gettime() {
      var whereYouAt = parseInt(myPlayer.currentTime());
     // alert(whereYouAt);
      return whereYouAt;
     // echo '<a onClick="play(15)"> TEST TAG 1</a>';
    }

    function addTimes() {
      times[times.length]=gettime();

      if (times.length == 2) {
        document.getElementById('tag_2').value = times[times.length-1];
        var play_2 = document.getElementById('play_2');
        play_2.disabled = false;
      }
      else if (times.length == 3) {
        document.getElementById('tag_3').value = times[times.length-1];
        var play_3 = document.getElementById('play_3');
        play_3.disabled = false;
      }
      else if (times.length == 4) {
        document.getElementById('tag_4').value = times[times.length-1];
        var play_4 = document.getElementById('play_4');
        play_4.disabled = false;
      }
    }

    function resetTags() {
        times.length=0; times[0] = 0; 
        document.getElementById('tag_1').value = 0;
        document.getElementById('tag_2').value = null;
        document.getElementById('tag_3').value = null;
        document.getElementById('tag_4').value = null;
        document.getElementById('play_2').disabled = true;
        document.getElementById('play_3').disabled = true;
        document.getElementById('play_4').disabled = true;
    }

    function playTAG($arr) {
      if (times[$arr]==null) {
        play(0);
      } else {
        play(times[$arr]);
      }
      
    }
</script>