<?php
  /* @var $this PostController */
  $this->pageTitle = CHtml::encode(Yii::app()->name)." - Video Submission";
?>

<head>
  <link href="http://vjs.zencdn.net/4.6/video-js.css" rel="stylesheet"> </link>
    <script src="http://vjs.zencdn.net/4.6/video.js"></script>
    <style type="text/css">
      .vjs-default-skin { color: #1b71a4; }
      .vjs-default-skin .vjs-play-progress,
      .vjs-default-skin .vjs-volume-level { background-color: #070808 }
      .vjs-default-skin .vjs-control-bar,
      .vjs-default-skin .vjs-big-play-button { background: rgba(232,226,226,0.7) }
      .vjs-default-skin .vjs-slider { background: rgba(232,226,226,0.2333333333333333) }
      .vjs-default-skin .vjs-control-bar { font-size: 127% }
    </style>
</head>

<!-- Page title -->
<div class="row">
  <div class="col-lg-offset-3 col-lg-9">
    <h1>
      <small><?= Yii::t('labels', 'Create a Great Video!'); ?></small>
    </h1>
  </div>
</div>

<!-- Gap -->
<div class="row">
  <div class="col-lg-12"></div>
</div>

<!-- Page content -->
<div class="row">
  <!-- Submenu -->
  <div class="col-lg-offset-1 col-lg-2 text-right">
    <ul class="nav nav-pills nav-stacked">
      <li class="active" id="submenu-new">
        <a href="#"><?= Yii::t('labels', 'New'); ?></a>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-drafts">
        <a data-toggle="collapse" href="#collapse-draft" id="nav-drop-drafts">
          <?= Yii::t('labels', 'Drafts'); ?> <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-drafts"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-draft">
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Draft'); ?> #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Draft'); ?> #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Draft'); ?> #3</a>
            </div>
          </li>
          <li class="media list-group-item text-center">
            <a href="#"><?= Yii::t('labels', 'See more'); ?></a>
          </li>
        </ul>
      </li>
    </ul>
    <ul class="nav nav-pills nav-stacked">
      <li id="submenu-recents">
        <a data-toggle="collapse" href="#collapse-recent" id="nav-drop-recents">
          <?= Yii::t('labels', 'Recents'); ?> <span class="glyphicon glyphicon-chevron-down" id="nav-drop-arrow-recents"></span>
        </a>
        <ul class="media-list text-left collapse" id="collapse-recent">
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Recent'); ?> #1</a>
            </div>
          </li>
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Recent'); ?> #2</a>
            </div>
          </li>
          <li class="media list-group-item">
            <div class="media-body">
              <a href="#"><?= Yii::t('labels', 'Recent'); ?> #3</a>
            </div>
          </li>
          <li class="media list-group-item text-center">
            <a href="#"><?= Yii::t('labels', 'See more'); ?></a>
          </li>
        </ul>
      </li>
    </ul>
  </div><!-- Submenu -->
  <!-- Main content -->
  <head>
    <script>
      var temp_upload;
    </script>
  </head>

  <div class="col-lg-9">
    <br>
    <?php
      $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array(
          'enctype' => 'multipart/form-data'
        ),
      ));
    ?>
    <fieldset>
      <?php
        // Title
        echo $form->textFieldGroup(
          $model,
          'title',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-lg-8',
            ),
            'id' => "video-title",
            'maxlength' => 300
          )
        ); // Title

        // Speech language
        echo $form->dropDownListGroup(
          $model,
          'lang_id',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-lg-8',
            ),
            'widgetOptions' => array(
              'data' => CHtml::listData($model_lang->findAll(), 'id', 'name'),
              'htmlOptions' => array(),
            )
          )
        ); // Speech language

        // Video
        echo $form->fileFieldGroup(
          $model,
          'video_file',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-lg-8',
            ),
            'hint' => Yii::t('labels', 'or')
          )
        );
      ?>

      <?php
        $this->widget('booster.widgets.TbButton',
          array(
            'label' => Yii::t('labels', 'Record now!'),
            'context' => 'primary',
            'htmlOptions' => array(
              'id' => 'btn_record',
              'class' => 'col-lg-offset-3',
              'data-toggle' => 'modal',
              'data-target' => '#recordModal',
            ),
          )
        );
      ?>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <?php echo $form->hiddenField($model, 'upload_file', array('id' => 'upload_filename')); ?>
          <?php echo $form->error($model, 'upload_file'); ?>
        </div>
      </div>
      
      <?php
        // Video
        
        // Description
        echo $form->ckEditorGroup(
          $model,
          'content',
          array(
            'wrapperHtmlOptions' => array(
              'class' => 'col-lg-8',
            ),
            'widgetOptions' => array(
              'editorOptions' => array(
                'fullpage' => 'js:true',
                'plugins' => 'basicstyles,toolbar,colorbutton,format,font,smiley,link,colordialog,enterkey,entities,floatingspace,wysiwygarea,indentlist,link,list,dialog,dialogui,button,indent,fakeobjects',
                /* 'width' => '640', */
                /* 'resize_maxWidth' => '640', */
                /* 'resize_minWidth' => '320'*/
              )
            )
          )
        ); // Description
      ?>
    </fieldset>

    <div class="form-group">
      <div class="col-lg-offset-3 col-lg-8">
        <?php
          $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => Yii::t('labels', 'Create'),
            'htmlOptions' => array('class' => 'pull-right')
          ));
        ?>
      </div>
    </div>

    <?php
      $this->endWidget();
    ?>
  </div><!-- Main content -->
</div>

<?php $this->beginWidget(
    'booster.widgets.TbModal',
    array('id' => 'recordModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?= Yii::t('labels', 'Record Your Video Now!'); ?></h4>
        <!--
      > Muaz Khan     - https://github.com/muaz-khan 
      > MIT License   - https://www.webrtc-experiment.com/licence/
      > Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
      -->
      <!-- script used for audio/video/gif recording -->
        <script src="https://www.webrtc-experiment.com/RecordRTC.js"> 
            
        </script>
    </div>
 
    <div style="text-align:center;" class="modal-body">
        <section class="experiment">  

        <p style="text-align:center;">
          <video class="video-js vjs-default-skin box" id="preview" controls style="border: 2px solid rgb(15, 158, 238); height: 320px; width: 480px;" poster="<?= Yii::app()->request->baseUrl; ?>/res/img/poster.png"></video> 
        </p>
        <hr />

        <button class="btn btn-default btn-small" id="record">  <span class="glyphicon glyphicon-record"></span> <?= Yii::t('labels', 'Record'); ?></button>
        <button class="btn btn-default btn-small" id="stop" disabled> <span class="glyphicon glyphicon-stop"> <?= Yii::t('labels', 'Stop'); ?></button>
        <button class="btn btn-default btn-small" id="delete" disabled> <span class="glyphicon glyphicon-trash"> <?= Yii::t('labels', 'Delete'); ?></button>

        <div id="container" style="padding:1em 2em;"></div>
            </section>
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'context' => 'primary',
                'label' => Yii::t('labels', 'Save'),
                'url' => '#',
                'id' => 'save',
                'htmlOptions' => array('data-dismiss' => 'modal', 'disabled'=>'true'),
            )
        ); ?>
        <?php $this->widget(
            'booster.widgets.TbButton',
            array(
                'label' => Yii::t('labels', 'Close'),
                 'id' => 'closemodal',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>

<?php
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/create.js', CClientScript::POS_END);
?>

<script> 
$(document).ready(function () {
$('#upload_filename').val("");//using the id of the textbox empty the value.
});
</script>

<script>
    // todo: this demo need to be updated for Firefox.
    // it currently focuses only chrome.
    
    function PostBlob(audioBlob, videoBlob, fileName) {
        var formData = new FormData();
        formData.append('filename', fileName);
        formData.append('audio-blob', audioBlob);
        formData.append('video-blob', videoBlob);
        xhr(<?= "'".Yii::app()->createUrl('site/save')."'" ?>, formData, function(ffmpeg_output) {
           // document.querySelector('h1').innerHTML = ffmpeg_output.replace( /\\n/g , '<br />');
            preview.src = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/uploads/temp/' + fileName + '-merged.webm';
            saveUpload.disabled=false;
            closeModal.disabled=false;
            deleteFiles.disabled = false;
            preview.play();
            preview.muted = false;
        });
    }

    var record = document.getElementById('record');
    var stop = document.getElementById('stop');
    var deleteFiles = document.getElementById('delete');
    var saveUpload = document.getElementById('save');

    var audio = document.querySelector('audio');

    var recordVideo = document.getElementById('record-video');
    var preview = document.getElementById('preview');

    var container = document.getElementById('container');

    var isFirefox = !!navigator.mozGetUserMedia;
    var closeModal = document.getElementById('closemodal');

    var recordAudio, recordVideo;
    record.onclick = function() {
        record.disabled = true;
        !window.stream && navigator.getUserMedia({
                audio: true,
                video: true
            }, function(stream) {
                window.stream = stream;
                onstream();
            }, function(error) {
                alert(JSON.stringify(error, null, '\t'));
            });

        window.stream && onstream();

        
    };

    function onstream() {
            preview.src = window.URL.createObjectURL(stream);
            preview.play();
            preview.muted = true;

            recordAudio = RecordRTC(stream, {
                // bufferSize: 16384,
                onAudioProcessStarted: function() {
                    if(!isFirefox) {
                        recordVideo.startRecording();
                    }
                }
            });
            
            recordVideo = RecordRTC(stream, {
                type: 'video'
            });
            
            recordAudio.startRecording();

            stop.disabled = false;
        }

    var fileName;
    stop.onclick = function() {
       /// document.querySelector('h1').innerHTML = 'Getting Blobs...';

        record.disabled = false;
        stop.disabled = true;
        closeModal.disabled=true;

        preview.src = '';
        preview.poster = <?= "'".Yii::app()->getBaseUrl()."'" ?>+'/images/' +'ajax-loader.gif';

        fileName = Math.round(Math.random() * 99999999) + 99999999;
        

        if (!isFirefox) {
            recordAudio.stopRecording(function() {
              //  document.querySelector('h1').innerHTML = 'Got audio-blob. Getting video-blob...';
                recordVideo.stopRecording(function() {
                   // document.querySelector('h1').innerHTML = 'Uploading to server...';
                    PostBlob(recordAudio.getBlob(), recordVideo.getBlob(), fileName);
                });
            });
        }
        
    };

    closeModal.onclick = function() {
        deleteAudioVideoFiles();
    };

    saveUpload.onclick = function() {
        document.getElementById('upload_filename').value=fileName + '-merged.webm';
    };

    deleteFiles.onclick = function() {
        deleteAudioVideoFiles();
    };

    function xhr(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
            }
        };
        request.open('POST', url);
        request.send(data);
    }

    function deleteAudioVideoFiles() {
        deleteFiles.disabled = true;

        if (!fileName) return;
        var formData = new FormData();
        formData.append('delete-file', fileName);
        xhr(<?= "'".Yii::app()->createUrl('site/delete')."'" ?>, formData, null, null, function(response) {
            console.log(response);
        });
        fileName = null;
        container.innerHTML = '';
        preview.poster = null;
        preview.src=null;
    }

</script>
