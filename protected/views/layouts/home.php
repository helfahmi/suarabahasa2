<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="bg-primary" id="home">
	<div class="row">
		<div class="col-lg-offset-1 col-lg-10">
			<!-- carousel -->
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<img class="center-block" src="<?= Yii::app()->request->baseUrl?>/res/img/feature1.png" width="80%">
						<div class="carousel-caption">
							<p>Speaking is the best way to practice your target language. Tell others a nice story in your target language and let native speaker share you their advices, so you can advance.</p>
						</div>
					</div>
					<div class="item">
						<img class="center-block" src="<?= Yii::app()->request->baseUrl?>/res/img/feature2.png" width="80%">
						<div class="carousel-caption">
							<p>Learning together is the best-practice of learning language. Watch others' video and share them your great advices to help them improve their skill and experience.</p>
						</div>
					</div>
					<div class="item">
						<img class="center-block" src="<?= Yii::app()->request->baseUrl?>/res/img/tes.png" width="80%">
						<div class="carousel-caption">
							<p>Telu</p>
						</div>
					</div>
				</div>

				<!-- Controls -->
				<a class="carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right-no-gradient carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div><!-- carousel -->
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-offset-1 col-lg-10">
			<div id="content">
				<?php echo $content; ?>
			</div><!-- content -->
		</div>
	</div>
</div>

<?php $this->endContent(); ?>