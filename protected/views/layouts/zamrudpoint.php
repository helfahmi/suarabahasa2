<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div class="container">
	<div class="row">
		<div class="col-lg-10">
			<div id="content">
				<?php echo $content; ?>
			</div><!-- content -->
		</div>
		<div class="col-lg-2">
			<div class="panel panel-primary" id="zamrud_point">
				<div class="panel-heading text-center">
					<strong><?= Yii::t('labels', 'Zamrud Point'); ?></strong>
				</div>
				<ul class="list-group text-center">
					<li class="list-group-item">
						<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_green.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/researcher.png" height="24" />
						<p class="no-space"><b><?= User::model()->findByAttributes(array('id'=> Yii::app()->user->getId()))->green_zamrud; ?> poin (siswa)</b></p>
						<p class="no-space">200 dari 1000</p>
					</li>
					<li class="list-group-item">
						<img src="<?= Yii::app()->baseUrl;?>/res/img/zamrud_red.png" width="24" /> <img src="<?= Yii::app()->baseUrl;?>/res/img/master.png" height="24" />
						<p class="no-space"><b><?= User::model()->findByAttributes(array('id'=> Yii::app()->user->getId()))->red_zamrud; ?> poin (guru)</b></p>
						<p class="no-space">99 dari 1000</p>
					</li>
				</ul>
			</div><!-- zamrud point -->
			<div class="text-center">
				<?=	CHtml::link(Yii::t('labels', 'Go to fame'), array('user/fame')); ?>
			</div>
		</div>
	</div>
</div>

<?php $this->endContent(); ?>