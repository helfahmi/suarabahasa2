<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
  
  <!-- meta files according to Bootstrap's website -->
  <meta content="IE=edge" http-equiv="X-UA-Compatible" />
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  
	<!-- blueprint CSS framework -->
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/zamrud.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- [for mobile] Group of brand and toggle -->
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= Yii::app()->request->baseUrl ?>">
        <img src="<?= Yii::app()->request->baseUrl?>/res/img/zamrud_logo_white.png" width="140px">
      </a>
      <!-- Button trigger modal -->
      <button class="btn btn-link btn-sm navbar-lang <?= (Yii::app()->user->isGuest) ? 'hidden' : '' ?>" data-toggle="modal" data-target="#myModal">
        <?= Yii::app()->user->hasState('_lang') ? "(".Yii::app()->user->getState('_lang').")" : "" ?>
      </button>
    </div><!-- [for mobile] Group of brand and toggle -->
    <div class="collapse navbar-collapse navbar-right" id="navbar-collapse">
      <?php if (Yii::app()->user->isGuest) { ?>
        <!-- guest -->
        <ul class="nav navbar-nav">
          <li>
            <?php
              echo CHtml::link(
                '<i class="fa fa-sign-in fa-lg"></i> '. Yii::t('user', 'Log In'),
                array('site/login')
              );
            ?>
          </li>
        </ul><!-- guest -->
      <?php } else { ?>
        <!-- logged in user -->
        <!-- practice menu -->
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-pencil-square-o fa-lg"></i> <?= Yii::t('labels', 'Practice') ?>
            </a>
            <ul class="dropdown-menu">
              <li>
                <?=
                  CHtml::link('
                      <span class="fa-stack">
                        <i class="fa fa-video-camera"></i>
                        <i class="fa fa-pencil fa-stack-1x"></i>
                      </span>
                    '. Yii::t('labels', 'Speaking'),
                    array('post/create')
                  );
                ?>
              </li>
              <li>
                <?=
                  CHtml::link('
                      <span class="fa-stack">
                        <i class="fa fa-film"></i>
                        <i class="fa fa-pencil fa-stack-1x"></i>
                      </span>
                    '. Yii::t('labels', 'Listening'),
                    array('postListens/quizes')
                  );
                ?>
              </li>
              <!--<li class="disabled">
                <a href="#">
                  <span class="fa-stack">
                    <i class="fa fa-file-text-o"></i>
                    <i class="fa fa-pencil fa-stack-1x"></i>
                  </span>
                  <?= Yii::t('labels', 'Writing') ?>
                </a>
              </li>
              <li class="disabled">
                <a href="#">
                  <span class="fa-stack">
                    <i class="fa fa-book"></i>
                    <i class="fa fa-pencil fa-stack-1x"></i>
                  </span>
                  <?= Yii::t('labels', 'Reading') ?>
                </a>
              </li>-->
            </ul>
          </li>
        </ul><!-- practice menu -->
        <!-- help other menu -->
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-check-square-o fa-lg"></i> <?= Yii::t('labels', 'Help Other') ?>
            </a>
            <ul class="dropdown-menu">
              <!--<li class="dropdown-header">
                <strong><?= Yii::t('labels', 'By Advice'); ?></strong>
              </li>-->
              <li>
                <?=
                  CHtml::link('
                      <span class="fa-stack">
                        <i class="fa fa-video-camera"></i>
                        <i class="fa fa-check fa-stack-1x"></i>
                      </span>
                    '. Yii::t('labels', 'Speaking'),
                    array('post/index')
                  );
                ?>
              </li>
              <!--<li class="disabled">
                <a href="#">
                  <span class="fa-stack">
                    <i class="fa fa-pencil"></i>
                    <i class="fa fa-check fa-stack-1x"></i>
                  </span>
                  <?= Yii::t('labels', 'Writing') ?>
                </a>
              </li>
              <li class="divider"></li>
              <li class="dropdown-header">
                <strong><?= Yii::t('labels', 'By Practice'); ?></strong>
              </li>-->
              <li>
                <?=
                  CHtml::link('
                      <span class="fa-stack">
                        <i class="fa fa-film"></i>
                        <i class="fa fa-check fa-stack-1x"></i>
                      </span>
                    '. Yii::t('labels', 'Listening'),
                   // array('postlistens/choose')
                    array('postListens/index')
                  );
                ?>
              </li>
              <!--<li class="disabled">
                <a href="#">
                  <span class="fa-stack">
                    <i class="fa fa-book"></i>
                    <i class="fa fa-check fa-stack-1x"></i>
                  </span>
                  <?= Yii::t('labels', 'Reading') ?>
                </a>
              </li>-->
            </ul>
          </li>
        </ul><!-- help other menu -->
        <!-- notification -->
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-globe fa-lg"></i> <?= Yii::t('labels', 'Notification') ?>
              <!-- <span class="badge">2</span> -->
            </a>
            <ul class="dropdown-menu">
              <li>
                <a class="pull-left" href="#">
                  <img src="<?php echo Yii::app()->request->baseUrl; ?>/res/img/user-anonymous-icon.png" width="auto" height="17" />
                  <span><?= Yii::t('labels', 'Nashir gave an advice...') ?> </span>
                </a>
              </li>
              <li>
                <a class="pull-left" href="#">
                  <img src="<?php echo Yii::app()->request->baseUrl; ?>/res/img/user-anonymous-icon.png" width="auto" height="17" />
                  <span><?= Yii::t('labels', 'Harits took your quiz...') ?> </span>
                </a>
              </li>
              <li class="text-center">
                <?=
                  CHtml::link(
                    '<strong>'.Yii::t('labels', 'See more').'</strong>',
                    array('user/notification')
                  );
                ?>
              </li>
            </ul>
          </li>
        </ul><!-- notification -->
        <!-- profile -->
        <ul class="nav navbar-nav navbar-right">
          <li>
            <?=
              CHtml::link(
                Yii::t('labels', 'Hi').', '.(isset(Yii::app()->user->name) ? Yii::app()->user->name : "User").'!',
                array('user/profile')
              );
            ?>
          </li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <img class="img-responsive" id="ppbar" src="<?php echo Yii::app()->request->baseUrl?>/uploads/account/<?= User::getPicture(Yii::app()->user->getId()) != '' ? User::getPicture(Yii::app()->user->getId()) : 'defaultpp.png' ?>" width="40" />
            </a>
            <ul class="dropdown-menu">
              <li>
                <?=
                  CHtml::link(
                    '<i class="fa fa-cog fa-fw"></i> '.Yii::t('labels', 'Setting'),
                    array('user/setting')
                  );
                ?>
              </li>
              <li>
                <?=
                  CHtml::link(
                    '<i class="fa fa-question fa-fw"></i> '.Yii::t('labels', 'Help'),
                    array('site/page/view/help')
                  );
                ?>
              </li>
              <li>
                <?=
                  CHtml::link(
                    '<i class="fa fa-info fa-fw"></i> '.Yii::t('labels', 'About'),
                    array('site/page/view/about')
                  );
                ?>
              </li>
              <li>
                <?=
                  CHtml::link(
                    '<i class="fa fa-coffee fa-fw"></i> '.Yii::t('labels', 'Credit'),
                    array('site/page/view/credit')
                  );
                ?>
              </li>
              <li class="divider"></li>
              <li>
                <a href="<?= Yii::app()->createUrl('site/logout') ?>">
                  <i class="fa fa-sign-out fa-fw"></i> <?= Yii::t('labels', 'Log Out') ?>
                </a>
              </li>
            </ul>
          </li>
        </ul><!-- profile --><!-- logged in user -->
      <?php } ?><!-- menu for logged in user and guest -->
    </div>
  </div>
</div>

<div id="wrap">
  <div id="main">
    <?php echo $content; ?>
  </div>
</div>

<div id="footer">
  <div class="col-lg-12">
    <div class="col-lg-offset-4 col-lg-1"><h4><small><?=  CHtml::link(Yii::t('site', 'Home'), array('site/index')); ?></small></h4></div>
    <div class="col-lg-1"><h4><small><?=  CHtml::link(Yii::t('site', 'About'), array('site/page/view/about')); ?></small></h4></div>
    <div class="col-lg-1"><h4><small><?=  CHtml::link(Yii::t('site', 'Help'), array('site/page/view/help')); ?></small></h4></div>
    <div class="col-lg-1"><h4><small><?=  CHtml::link(Yii::t('site', 'Credit'), array('site/page/view/credit')); ?></small></h4></div>
  </div>
  <div class="col-lg-12">
    <small>
      Copyright &copy; <?php echo date('Y'); ?> by NAH!.<br/>
      All Rights Reserved.<br/>
      <?php echo Yii::powered(); ?>
    </small>
  </div>
</div><!-- footer -->

<div class="container" id="page">
  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel"><?= Yii::t('labels', 'Choose your language') ?></h4>
        </div>
        <div class="modal-body">
          <a href="<?= Yii::app()->createUrl("site/changeLang", array('lang' => 'en')) ?>">English</a><br/>
          <a href="<?= Yii::app()->createUrl("site/changeLang", array('lang' => 'id')) ?>">Bahasa Indonesia</a><br/>
          <a href="<?= Yii::app()->createUrl("site/changeLang", array('lang' => 'az')) ?>">Azərbaycan dili</a><br/>
          <a href="<?= Yii::app()->createUrl("site/changeLang", array('lang' => 'my')) ?>">Bahasa Melayu</a><br/>
          <a href="#">Basa Jawa</a><br/>
          <a href="#">Bisaya</a><br/>
          <a href="#">Filipino</a><br/>
          <a href="#">Tiếng Việt</a>
        <br/></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('labels', 'Close'); ?></button>
        </div>
      </div>
    </div>
  </div><!-- Modal -->
</div><!-- page -->

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<script>
function testAPI() {
  console.log('Welcome!  Fetching your information.... ');
  FB.api('/me', function(response) {
    console.log(response);
    
    $.post("<?= Yii::app()->request->baseUrl ?>/site/checkLogin", function(data){
      if(data == "false"){
        redirect('<?= Yii::app()->request->baseUrl ?>/site/login?fb=1', response);
      }
    });

    console.log('Successful login for: ' + response.name);
  });
}
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fb.js"></script>

</body>
</html>
