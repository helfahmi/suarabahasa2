<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/zamrudpoint'); ?>

<div class="col-lg-offset-2 col-lg-10">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>

<?php $this->endContent(); ?>