<?php $this->pageTitle=Yii::app()->name . ' - Log In'; ?>

<div class="row">
  <div class="col-lg-12 box effect2">
    <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
      'id'=>'login-form',
      'enableClientValidation'=>true,
      'clientOptions'=>array(
          'validateOnSubmit'=>true,
      ),
      'htmlOptions' => array('class' => 'col-lg-offset-3 col-lg-6 well')
    )); ?>

    <legend><?= Yii::t('user', 'Log In'); ?></legend>

    <?php echo $form->textFieldGroup($model,'username'); ?>

    <?php echo $form->passwordFieldGroup($model,'password'); ?>

    <?php echo $form->checkBoxGroup($model,'rememberMe'); ?>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary pull-right"><?= Yii::t('user', 'Log In'); ?></button>
      <a class="btn btn-link pull-right" href="<?= Yii::app()->createUrl('site/register') ?>"><?= Yii::t('user', 'Register'); ?></a>
    </div>

    <?php $this->endWidget(); ?>

    <div class="col-lg-offset-3 col-lg-6 text-center">
      <div class="col-lg-12 hr-container">
        <hr class="hr-line">
        <span class="hr-text"><strong><?= Yii::t('common', 'or'); ?></strong></span>
      </div>
      <div class="col-lg-4">
        <?php
          $this->widget('booster.widgets.TbButton', array(
            'block' => true,
            'encodeLabel' => false,
            'buttonType' => 'submit',
            'context' => 'info',
            'label' => '<i class="fa fa-facebook-square fa-lg"></i> Facebook',
            'htmlOptions' => array('onclick'=>'checkLoginState();'),
          ));
        ?>
      </div>
      <div class="col-lg-4">
        <?php
          $this->widget('booster.widgets.TbButton', array(
            'block' => true,
            'encodeLabel' => false,
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => '<i class="fa fa-twitter-square fa-lg"></i> Twitter',
          ));
        ?>
      </div>
      <div class="col-lg-4">
        <?php
          $this->widget('booster.widgets.TbButton', array(
            'block' => true,
            'encodeLabel' => false,
            'buttonType' => 'submit',
            'context' => 'danger',
            'label' => '<i class="fa fa-google-plus-square fa-lg"></i> Google+',
          ));
        ?>
      </div>
      </div>
    </div>
  </div>
</div>