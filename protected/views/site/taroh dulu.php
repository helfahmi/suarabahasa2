<ul class="media-list panel-comment">
          <li class="media">
            <div class="row">
              <!-- Audio title -->
              <h5 class="col-sm-2 text-right">
                Speech #1
              </h5><!-- Audio title -->
              <!-- Audio -->
              <div class="col-sm-10 media-body">
                <?php
                  $this->widget(
                    'ext.aii-audio-player.AiiAudioPlayer',
                    array(
                      'playerID' => 'mp1',
                      'trackOptions' => array('soundFile' => 'evergreen.mp3,evergreen2.mp3', 'alternative' => 'nothing'),
                      'flashPlayerOptions' => array('width' => '100%'),
                      'mp3Folder' => 'res/aud',
                    )
                  );
                ?>
              </div><!-- Audio -->
            </div>
            <div class="row">
              <!-- Correction buttons -->
              <div class="col-sm-offset-2 col-sm-10">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-primary btn-xs">
                    <input type="radio" name="options" id="option1"> Perfect
                  </label>
                  <label class="btn btn-primary btn-xs">
                    <input type="radio" name="options" id="option2"> Advice
                  </label>
                </div>
                <button type="button" class="btn btn-default btn-xs">Record advice</button>
                <button type="button" class="btn btn-default btn-xs">Write advice</button>
                <!-- Correction buttons -->
              </div>
            </div>
          </li>
          <li class="media">
            <div class="row">
              <h5 class="col-sm-2 text-right">
                Speech #2
              </h5>
              <div class="col-sm-10 media-body">
                <?php
                  $this->widget(
                    'ext.aii-audio-player.AiiAudioPlayer',
                    array(
                      'playerID' => 'mp2',
                      'trackOptions' => array('soundFile' => 'evergreen2.mp3', 'alternative' => 'nothing'),
                      'flashPlayerOptions' => array('width' => '100%'),
                      'mp3Folder' => 'res/aud',
                    )
                  );
                ?>
              </div>
            </div>
            <div class="row">
              <!-- Correction buttons -->
              <div class="col-sm-offset-2 col-sm-10">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-primary btn-xs">
                    <input type="radio" name="options" id="option1"> Perfect
                  </label>
                  <label class="btn btn-primary btn-xs">
                    <input type="radio" name="options" id="option2"> Advice
                  </label>
                </div>
                <button type="button" class="btn btn-default btn-xs">Record advice</button>
                <button type="button" class="btn btn-default btn-xs">Write advice</button>
                <!-- Correction buttons -->
              </div>
            </div>
          </li>
        </ul><!-- Audio correction -->