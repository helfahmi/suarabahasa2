<?php
	/* @var $this SiteController */
	$this->pageTitle = Yii::app()->name;
?>

<?php if (Yii::app()->user->isGuest) { ?>
	<!-- summary -->
	<div class="row text-center space-top">
		<div class="col-lg-offset-2 col-lg-8">
			<h1><i><?= CHtml::encode(Yii::app()->name); ?> <?= Yii::t('home', 'boosts your spoken language experience practically.'); ?></i></h1>
		</div>
	</div><!-- summary -->
	<!-- overview -->
	<div class="row space-top">
		<div class="col-lg-6 text-center">
			<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/speaking.png" alt="Speaking Practice" width="33%">
			<h3><?= Yii::t('home', 'Tell your great stories in your target language.'); ?></h3>
		</div>
		<div class="col-lg-6 text-center">
			<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/listening.png" alt="Listening Practice" width="33%">
			<h3><?= Yii::t('home', 'Listen to your target language spoken by native speaker.'); ?></h3>
		</div>
	</div>
	<div class="row space-top">
		<div class="col-lg-6 text-center">
			<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/advice.png" alt="Speaking Help Others" width="33%">
			<h3><?= Yii::t('home', 'Share your advices with others to help them improving their skill.'); ?></h3>
		</div>
		<div class="col-lg-6 text-center">
			<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/quiz.png" alt="Listening Help Others" width="33%">
			<h3><?= Yii::t('home', 'Create listening quizes to help others learning your native language.'); ?></h3>
		</div>
	</div>
	<div class="row space-top">
		<div class="col-lg-offset-4 col-lg-4">
			<?= CHtml::link(Yii::t('user', 'Register now!'), array('site/register'), array('class' => 'btn btn-primary btn-lg btn-block')); ?>
		</div>
	</div><!-- overview -->
<?php } else { ?>
	<div class="row" id="pagenotitle">
		<div class="col-lg-12 tit_tutrial text-center">
			<b><?= Yii::t('labels', 'What can you do with'); ?> <?= CHtml::encode(Yii::app()->name); ?>?</b>
		</div>
		<div class='col-lg-12 image_body_list' width="100%">
			<div class="col-lg-3 text-center">
				<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/speaking_abu.png" alt="Speaking Practice" width="55%">
				<br><br><?= CHtml::link(Yii::t('labels', 'Practice Speaking'), array('post/create'), array('class' => 'btn btn-primary btn-sm')); ?>
				<h6><?= Yii::t('labels', 'Tell your great stories in your target language'); ?></h6>
			</div>
			<div class="col-lg-3 text-center">
				<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/listening_abu.png" alt="Listening Practice" width="55%">
				<br><br><?= CHtml::link(Yii::t('labels', 'Practice Listening'), array('postListen/index'), array('class' => 'btn btn-primary btn-sm')); ?>
				<h6><?= Yii::t('labels', 'Listen to your target language spoken by native speaker'); ?></h6>
			</div>
			<div class="col-lg-3 text-center">
				<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/advice_abu.png" alt="Speaking Help Others" width="55%">
				<br><br><?= CHtml::link(Yii::t('labels', 'Help Others Speaking'), array('post/index'), array('class' => 'btn btn-primary btn-sm')); ?>
				<h6><?= Yii::t('labels', 'Share your advices with others to help them improving their skill'); ?></h6>
			</div>
			<div class="col-lg-3 text-center">
				<img src="<?= Yii::app()->request->baseUrl; ?>/res/img/quiz_abu.png" alt="Listening Help Others" width="55%">
				<br><br><?= CHtml::link(Yii::t('labels', 'Help Others Listening'), array('postListen/index'), array('class' => 'btn btn-primary btn-sm')); ?>
				<h6><?= Yii::t('labels', 'Create listening quizes to help others learning your native language'); ?></h6>
			</div>
		</div>
	</div><!-- pagenotitle -->
	<div class="row" id="pagecontent">
		<div class="col-lg-12 space-top">
			<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
				<li class="active"><a href="#speaking" data-toggle="tab"><?= Yii::t('labels', 'Speaking'); ?></a></li>
				<li><a href="#listening" data-toggle="tab"><?= Yii::t('labels', 'Listening'); ?></a></li>
			</ul>
			<div id="my-tab-content" class="tab-content">
				<div class="tab-pane active" id="speaking">
					<?= CHtml::link(Yii::t('user', '<span class="fa-stack">
					<i class="fa fa-video-camera"></i>
					<i class="fa fa-pencil fa-stack-1x"></i>
					</span>'.Yii::t('labels', 'Create Video for Practice Speaking')), array('post/create'), array('class' => 'btn btn-warning btn-sm pull-right space-top')); ?>
					<div class="col-lg-12 space-top">
						<ul class="media-list">
							<!-- Video -->
							<?php
								foreach($models as $model) {
									$model_advices = Advice::model()->findAll('post_id = :post_id', array(':post_id' => $model->id));
									$video_thumb = Videos::model()->findByAttributes(array('post_id'=> $model->id))->thumbnail;
									$komen = count($model_advices);
							?>
							<li class="media media-list-highlight">
								<a class="pull-left" href="#">
									<img class="media-object"  src="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail/<?php echo $video_thumb?>" alt="video thumbnail" width="144" height="96">
								</a>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
										<small><?= Yii::t('labels', 'More');?> <span class="glyphicon glyphicon-collapse-down"></span></small>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href=""><small><?= Yii::t('labels', 'Mark as spam'); ?></small></a>
										</li>
									</ul>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php echo Yii::app()->request->baseUrl?>/post/correct?post_id=<?= $model->id ?>"><?= Posts::getTitle($model->id) ?></a>
										<a href=""><small><?= Posts::getLang($model->id); ?></small></a><br>
									</h4>
									<div class="media-subheading"><small><?= Yii::t('labels', 'by'); ?> <a href=""><?= User::getFullName($model->user_id) ?></a></small></div>
									<div class="media-content"><?= Posts::getContent($model->id) ?></div>
									<div class="media-footer">
										<a><i class="fa fa-thumbs-o-up"></i> <small><?= Posts::getLike($model->id) ?> <?= Yii::t('labels', 'vote up'); ?></small></a>
										<span class="tab"></span>
										<a ><i class="fa fa-thumbs-o-down"></i> <small><?= Posts::getDislike($model->id) ?> <?= Yii::t('labels', 'vote down'); ?></small></a>
										<span class="tab"></span>
										<a ><i class="fa fa-user"></i> <small><?= Posts::getWatch($model->id) ?> <?= Yii::t('labels', 'watch'); ?></small></a>
										<span class="tab"></span>
										<a ><i class="fa fa-quote-right"></i> <small><?= $komen ?> <?= Yii::t('labels', 'advice'); ?></small></a>
										<span class="tab"></span>
										<span><small><?= Posts::getDate($model->id) ?></small></span>
									</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
				<div class="tab-pane" id="listening">
					<?= CHtml::link(Yii::t('user', '  <span class="fa-stack">
					<i class="fa fa-film"></i>
					<i class="fa fa-pencil fa-stack-1x"></i>
					</span>'.Yii::t('labels', 'Create Video to Help Other Listening')), array('post/create'), array('class' => 'btn btn-warning btn-sm pull-right space-top')); ?>
					<div class="col-lg-12 space-top">
						<ul class="media-list">
							<!-- Video -->
							<?php
								foreach($model_listens as $model_listen) {
									//$model_advices = Advice::model()->findAll('post_id = :post_id', array(':post_id' => $model->id));
									$video_thumb = VideoListens::model()->findByAttributes(array('post_listen_id'=> $model_listen->id))->thumbnail;
									//$komen = count($model_advices);
							?>
							<li class="media media-list-highlight">
								<a class="pull-left" href="#">
									<img class="media-object"  src="<?php echo Yii::app()->request->baseUrl?>/uploads/thumbnail_listens/<?php echo $video_thumb?>" alt="video thumbnail" width="144" height="96">
								</a>
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
										<small><?= Yii::t('labels', 'More');?> <span class="glyphicon glyphicon-collapse-down"></span></small>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li>
											<a href=""><small><?= Yii::t('labels', 'Mark as spam'); ?></small></a>
										</li>
									</ul>
								</div>
								<div class="media-body">
									<h4 class="media-heading">
										<a href="<?php echo Yii::app()->request->baseUrl?>/postListens/quizes?id=<?= $model_listen->id ?>"><?= PostListens::getTitle($model_listen->id) ?></a>
										<a href=""><small><?= PostListens::getLang($model_listen->id); ?></small></a><br>
									</h4>
									<div class="media-subheading"><small><?= Yii::t('labels', 'by'); ?> <a href=""><?= User::getFullName($model_listen->user_id) ?></a></small></div>
									<div class="media-content">
										Theme: <?= Themes::model()->findByAttributes(array('id'=> $model_listen->theme_id))->name ?> 
										<br>
										Topic: <?= Topics::model()->findByAttributes(array('id'=> $model_listen->topic_id))->name ?> 
									</div>
									<div class="media-footer">
										<a ><i class="fa fa-thumbs-o-up"></i> <small><?= PostListens::getLike($model_listen->id) ?> <?= Yii::t('labels', 'vote up'); ?></small></a>
										<span class="tab"></span>
										<a ><i class="fa fa-thumbs-o-down"></i> <small><?= PostListens::getDislike($model_listen->id) ?> <?= Yii::t('labels', 'vote down'); ?></small></a>
										<span class="tab"></span>
										<a ><i class="fa fa-user"></i> <small>- <?= Yii::t('labels', 'watch'); ?></small></a>
										<span class="tab"></span>
										<span><small><?= Posts::getDate($model->id) ?></small></span>
									</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!-- pagecontent -->
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$('#tabs').tab();
		});
	</script>
<?php } ?>