<?php
	/* @var $this SiteController */

	$this->pageTitle=Yii::app()->name . ' - About';
	$this->breadcrumbs=array(
		'About',
	);
?>

<div class="col-lg-12" id="pagetitle">
	<h1>About</h1>
</div><!-- pagetitle -->
<div class="col-lg-12 text-justify" id="pagecontent">
	<p><?= Yii::app()->name.' '.Yii::t('site', 'provides you great features for learning languages. You can practice your speaking, listening, writing, and reading skill online and show it to native speaker in an instant. You also get a bunch of useful advices from native speaker about the language. On the other hand, you can help others learning your native language by giving an advice or creating an exercise for them to do. Register now and talk globally!'); ?></p>
	<hr>
	<h5><b><?= Yii::t('site', 'Developer Team'); ?></b></h5>
	<div>Nashir <a>(13511044@std.stei.itb.ac.id)</a></div>
	<div>SAHE <a>(13511046@std.stei.itb.ac.id)</a></div>
	<div>Alifa <a>(13511074@std.stei.itb.ac.id)</a></div>
</div><!-- pagecontent -->
