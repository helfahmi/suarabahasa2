<?php
	/* @var $this SiteController */

	$this->pageTitle=Yii::app()->name . ' - Credit';
	$this->breadcrumbs=array(
		'Credit',
	);
?>

<div class="col-lg-12" id="pagetitle">
	<h1><?= Yii::t('site', 'Credit'); ?></h1>
</div><!-- pagetitle -->
<div class="col-lg-12 text-justify" id="pagecontent">
	Check Mark designed by <a href="http://www.thenounproject.com/aguycalledgary">aguycalledgary</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Paint Palette designed by <a href="http://www.thenounproject.com/aimee.sferreira">Aimeê Ferreira</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Comment designed by <a href="http://www.thenounproject.com/akikonute">Akiko Kunugi</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Safety Glasses designed by <a href="http://www.thenounproject.com/zangetron">Alex Z</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Quote designed by <a href="http://www.thenounproject.com/Anisha Varghese">Anisha Varghese</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	User designed by <a href="http://www.thenounproject.com/bjorna1">Björn Andersson</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Customer Service designed by <a href="http://www.thenounproject.com/loyomo">carlotta zampini</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Food designed by <a href="http://www.thenounproject.com/hivernoir">Claire Jones</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Alligator designed by <a href="http://www.thenounproject.com/clf8748">Corey Felter</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Suitcase designed by <a href="http://www.thenounproject.com/diegonaive">Diego Naive</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Shark designed by <a href="http://www.thenounproject.com/Ealancheliyan">Ealancheliyan s</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Racquet designed by <a href="http://www.thenounproject.com/edward">Edward Boatman</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Video Camera designed by <a href="http://www.thenounproject.com/flayks">Félix Péault</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Television designed by <a href="http://www.thenounproject.com/Javiercalvo1985">Javier Calvo Patiño</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Child Care designed by <a href="http://www.thenounproject.com/jerrywang.ntu">Jerry Wang</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Quotes designed by <a href="http://www.thenounproject.com/kattyk">Kathryn Hing</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	User designed by <a href="http://www.thenounproject.com/luigidicapua85">Luigi Di Capua</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Headphones designed by <a href="http://www.thenounproject.com/Madebyelvis">Madebyelvis</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Customer Service designed by <a href="http://www.thenounproject.com/MRFADesigns">Martha Ormiston</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Institution designed by <a href="http://www.thenounproject.com/parasamuralikrishna">Murali Krishna</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Speech Bubble designed by <a href="http://www.thenounproject.com/parasamuralikrishna">Murali Krishna</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Female designed by <a href="http://www.thenounproject.com/nashadabdu">Nashad Abdu</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Thumbs Up designed by <a href="http://www.thenounproject.com/fencehopping">Nick Holroyd</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Puzzle designed by <a href="http://www.thenounproject.com/tilllur">Nurutdinov Timur</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Microphone designed by <a href="http://www.thenounproject.com/parkerdesigns">Parker Foote</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Girl designed by <a href="http://www.thenounproject.com/dreamer810">Qing Li</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Doctor designed by <a href="http://www.thenounproject.com/Simon Child">Simon Child</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Indonesia designed by <a href="http://www.thenounproject.com/freevectormaps">Ted Grajeda</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	Rihanna designed by <a href="http://www.thenounproject.com/likh.v">Vlad Likh</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.<br>
	User designed by <a href="http://www.thenounproject.com/wilsonjoseph">Wilson Joseph</a> from the <a href="http://www.thenounproject.com">Noun Project</a>.
</div><!-- pagecontent -->
