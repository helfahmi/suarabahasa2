<?php
  $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
    'id' => 'register-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('enctype' => 'multipart/form-data', 'class'=>'col-lg-offset-2 col-lg-8 well'),
  ));
?>
  
  <legend><?= Yii::t('user', 'Registration'); ?></legend>

  <?php echo $form->errorSummary($model); ?>
  
  <?php echo $form->textFieldGroup($model,'username', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 45,
      ),
      'hint' => Yii::t('common', 'Max.').' 45 '.Yii::t('common', 'characters').'.'
  )); ?>

  <?php echo $form->passwordFieldGroup($model,'password', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 255,
      )
  )); ?>

  <?php echo $form->passwordFieldGroup($model,'password_repeat', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 255,
      )
  )); ?>

  <?php echo $form->textFieldGroup($model,'full_name', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 100,
      ),
      'hint' => Yii::t('common', 'Max.').' 100 '.Yii::t('common', 'characters').'.'
  )); ?>

  <?php echo $form->datePickerGroup($model, 'ttl', array(
        'widgetOptions' => array(
          'options' => array(
            'language' => 'en',
          ),
          'htmlOptions' => array('class' => 'datepicker'),
        ),
        'wrapperHtmlOptions' => array(
          'class' => '',
        ),
        'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
      ));
  ?>
  
  <?php echo $form->textFieldGroup($model,'email', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 2255,
      )
  )); ?>
  
  <?php echo $form->fileFieldGroup($model,'picture', array(
      'wrapperHtmlOptions' => array(
        'class' => '',
        'maxlength' => 128,
      )
  )); ?>

  <div class="form-actions col-md-offset-3">
    <?php $this->widget('booster.widgets.TbButton', array(
      'buttonType'=>'submit',
      'context'=>'primary',
      'label'=> Yii::t('user', 'Register'),
      'htmlOptions' => array('class' => 'pull-right')
    )); ?>
  </div>

<?php $this->endWidget(); ?>

<script>
  $('.datepicker').datepicker({
      format: 'dd-mm-yyyy'
  });
</script>