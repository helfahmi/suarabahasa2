<?php

class PostController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
	public function actions()
    {
        return array(
            'upload'=>array(
                'class'=>'xupload.actions.XUploadAction',
                'path' =>Yii::app() -> getBasePath() . "/../uploads",
                'publicPath' => Yii::app() -> getBaseUrl() . "/uploads",
            ),
        );
    }

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','preview','record','hapus','save','tag','correct','index'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Post']))
		{
			$model->attributes=$_POST['Post'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionCreate()
	{
		$this->layout = 'sidebar-zamrudpoint';

		$model = new PostForm;
		$model_lang = new Lang;
		
		if (isset($_POST['PostForm']))
		{	
			 
			$model->attributes = $_POST['PostForm'];
			if ($model->validate())
			{ 
				$post = new Posts;
				$video = new Videos;
				$post->attributes = $_POST['PostForm'];
				
				$post->date = date("Y-m-d G:i:s");
				$post->user_id = (Yii::app()->user->getId());
				$post->zamrud_points = 0;
				$post->status = 0;

				if ($_POST['PostForm']['upload_file'] == null) {
					$video->file = CUploadedFile::getInstance($model, 'video_file');	
				}
				$video->title = $post->title;
				$video->user_id = (Yii::app()->user->getId());
				$video->save(false);

				
				if ($post->save()) {
					if ($_POST['PostForm']['upload_file'] == null) {
						$fileName = $video->id.'-'. (Yii::app()->user->getId()).'.'.$video->file->extensionName;
						$path = getcwd().'/uploads/videos/'.$fileName;
						$video->file->saveAs($path);
						$video->post_id = $post->id;
						$video->file = $fileName;
						$post->save(false); //no validation run
						
					} else {
						$fileName = $video->id.'-'. (Yii::app()->user->getId()).'.webm';
						$video->post_id = $post->id;
						$pathFrom = Yii::app( )->getBasePath( )."/../uploads/temp/";
						$pathTo = Yii::app( )->getBasePath( )."/../uploads/videos/";
						rename($pathFrom.$_POST['PostForm']['upload_file'], $pathTo.$fileName);

						 if( !is_dir( $pathFrom ) ) {
				            mkdir( $pathFrom );
				            chmod( $pathFrom, 0777 );
				        }
						$video->file = $fileName;
						$post->save(false); //no validation run

					}
					
					$second = 2; //specify the time to get the screen shot at (can easily be randomly generated)
					$image = 'uploads/thumbnail/'.$video->id.'-'. (Yii::app()->user->getId()).'.jpg';
					$videoname = 'uploads/videos/'.$fileName;
					//finally assemble the command and execute it

					
	                $cmd = "ffmpeg -i $videoname -deinterlace -an -ss $second -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $image 2>&1";

					shell_exec($cmd);
					
					$video->thumbnail = $video->id.'-'. (Yii::app()->user->getId()).'.jpg';
					$video->save(false);
					$video_id = $video->id;

					$this->redirect(array('/post/preview','video_id'=>$video_id));
				} else {
					throw new CHttpException(405, CHtml::errorSummary($post));
				} 
			} else {
				 //echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
				if (($_POST['PostForm']['upload_file'] !== "")) {
					$pathFrom = Yii::app( )->getBasePath( )."/../uploads/temp/";
					if (file_exists ( $pathFrom.$_POST['PostForm']['upload_file'])) {
						unlink($pathFrom.$_POST['PostForm']['upload_file']);
					}
				}
				
			}
		}
		unset($_POST);
		$this->render('create',array('model'=>$model,'model_lang'=>$model_lang,));
	}

	public function actionPreview() 
	{
			
	   $video_id = $_GET['video_id'];

	   $command=Yii::app()->db->createCommand(
			"SELECT * FROM videos WHERE id=:ID");
        $command->bindParam(":ID", $video_id, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();
        $video_file = $row['file'];
        $video_thumb = $row['thumbnail'];
        $post_id = $row['post_id'];
        $title = $row['title'];

        $command=Yii::app()->db->createCommand(
			"SELECT * FROM posts WHERE id=:ID");
        $command->bindParam(":ID", $post_id, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();
        $content = $row['content'];


	   //$post=Yii::app()->session->get('postForm');
       $model_tag = new TagForm;

       if (isset($_POST['TagForm'])) {
       		//echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
       		$tag1 = new Tags;
        	$tag1->setIsNewRecord(true);  // To make sure that this would add new record and not update
          	$tag1->video_id = $video_id;
          	$tag1->time_tag = 0;
          	$tag1->save();


			if (isset($_POST['TagForm']['tag_2'])) {
				//echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
	       		$tag2 = new Tags;
	        	$tag2->setIsNewRecord(true);  // To make sure that this would add new record and not update
	          	$tag2->video_id = $video_id;
	          	$tag2->time_tag = $_POST['TagForm']['tag_2'];
	          	$tag2->save();

			}

			if (isset($_POST['TagForm']['tag_3'])) {
				//echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
	       		$tag3 = new Tags;
	        	$tag3->setIsNewRecord(true);  // To make sure that this would add new record and not update
	          	$tag3->video_id = $video_id;
	          	$tag3->time_tag = $_POST['TagForm']['tag_3'];
	          	$tag3->save();

			}

			if (isset($_POST['TagForm']['tag_4'])) {
				//echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
	       		$tag4 = new Tags;
	        	$tag4->setIsNewRecord(true);  // To make sure that this would add new record and not update
	          	$tag4->video_id = $video_id;
	          	$tag4->time_tag = $_POST['TagForm']['tag_4'];
	          	$tag4->save();

			}

			$model=Posts::model()->findByPk((int)$post_id);
	        $model->status=1;
	        $model->save();
			$this->redirect(array('/site/index'));
       }

       
		   

		$this->render('preview',array('model_tag'=>$model_tag,'video_file'=>$video_file,'content'=>$content,'title'=>$title,'post_id'=>$post_id,'video_thumb'=>$video_thumb));
	}

	public function actionRecord() 
	{
   		$this->renderPartial('record',array(),false,true);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Post('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionSave(){
		// Muaz Khan     - www.MuazKhan.com 
		// MIT License   - https://www.webrtc-experiment.com/licence/
		// Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
		foreach(array('video', 'audio') as $type) {
			if (isset($_FILES["${type}-blob"])) {
			
				echo 'uploads/temp/';
				
				$fileName = $_POST["${type}-filename"];
				$uploadDirectory = 'uploads/temp/'.$fileName;
				
				if (!move_uploaded_file($_FILES["${type}-blob"]["tmp_name"], $uploadDirectory)) {
					echo(" problem moving uploaded file");
				}

				echo($fileName);
			}
		}
	}
	
	public function actionHapus(){
		// Muaz Khan     - www.MuazKhan.com 
		// MIT License   - https://www.webrtc-experiment.com/licence/
		// Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
		if (isset($_POST['delete-file'])) {
			$fileName = 'uploads/temp'.$_POST['delete-file'];
			if(!unlink($fileName.'.webm') || !unlink($fileName.'.wav')) {
				echo(' problem deleting files.');
			}
			else {
				echo(' both wav/webm files deleted successfully.');
			}
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Post the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Post::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Post $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='post-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionTag() {
		echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
		/*
		$tags = new Tags;
		$tags->setIsNewRecord(true);  // To make sure that this would add new record and not update
     	$tags->post_id = $post_id;
     	$tags->time = $time;
     	$tags->save();
     	*/
	}

	/**
	*  Displays the correction page
	*/
	public function actionCorrect($post_id)
	{
		$this->layout = 'normal-zamrudpoint';

	  	$model_post=Posts::model()->findByPk((int)$post_id);
	  	$model_video = Videos::model()->find('post_id = :post_id', array(':post_id' => $post_id));


	  	$models = Advice::model()->findAll('post_id = :post_id', array(':post_id' => $post_id));
	  	$model = new Advice;

	  	$model_tags = Tags::model()->findAll('video_id = :video_id', array(':video_id' => $model_video->id));
	  	$model_tag = new Tags;

	  	if(isset($_POST['Advice'])){
			$model->attributes=$_POST['Advice'];
			unset($_POST['Advice']);
			
			$model->user_id = Yii::app()->user->id;
			$model->post_id = $post_id;
			$model->date = date("Y-m-d");
			$model->zamrud_points = 0;

			// validate user input and redirect to the previous page if valid
			if($model->save()){
				$this->redirect(array('/post/correct','post_id'=>$post_id)); //<-- harusnya jg pake post id lagi
			}
		} else {
			$watchs = Posts::getWatch($model_post->id) + 1;
			$model_post->watchs = $watchs;
			$model_post->save();
		}

	    $this->render('correct', array('model'=>$model, 'models' => $models, 'model_post'=>$model_post, 'model_video'=>$model_video, 
	    	'model_tags'=>$model_tags, 'model_tag'=>$model_tag));
	}

	  public function actionIndex() {
	  	$this->layout = 'normal-zamrudpoint';
	  	
	  	$models = Posts::model()->findAll('status = :status', array(':status' => 1));
	  	$model = new Posts;
	  	

	  	$this->render('index', array('model'=>$model, 'models' => $models));
	  }
	
}
