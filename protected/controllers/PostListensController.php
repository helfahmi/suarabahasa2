<?php

class PostListensController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','plan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),*/
		);
	}

	public function actionQuizes(){

		$id = Yii::app()->request->getQuery("id", -1);

		if($id == -1){
			$this->layout = 'normal-zamrudpoint';

			$models = PostListens::model()->findAll();
			$model = new PostListens;

			$this->render('quiz',array(
				'model'=>$model,
				'models'=>$models,
			));	
		} else {
			$this->layout = 'normal-zamrudpoint';

			$answer = new Answer;
		  	$model_post_listen = PostListens::model()->findByPk($id);
		  	$model_video_listen = VideoListens::model()->find('post_listen_id = :post_listen_id', array(':post_listen_id' => $id));
			$questions = Questions::model()->findAll('story_id = :story_id', array(':story_id' => $model_post_listen->story_id), array('limit'=>4));

			if(isset($_POST['Answer'])){
				$answer->attributes=$_POST['Answer'];
				if($answer->validate()){
					$score = 0;
					
					//echo count($questions); die();
					for($i=0;$i<count($questions);$i++){
						if($answer['answer'. ($i + 1)] == ($questions[$i]->answer - 1)){
							$score++;
						}	
					}

					$quiz_result = new Quiz;
					$quiz_result->post_listen_id = $id;
					$quiz_result->user_id = Yii::app()->user->id;
					$quiz_result->score = $score * 25;
					$quiz_result->date = date("Y-m-d H:i:s");
					$quiz_result->save();

					$this->render('score', array('model_post_listen'=>$model_post_listen, 'score'=>$quiz_result->score));
				} else {
					$this->render('view', array('model_post_listen'=>$model_post_listen, 'model_video_listen'=>$model_video_listen, 'questions'=>$questions, 'answer'=>$answer, 'error'=>$error));
				}
					
			} else {

			    $this->render('view', array(
			    	'model_post_listen'=>$model_post_listen, 'model_video_listen'=>$model_video_listen, 'questions'=>$questions, 'answer'=>$answer));
			}
		}
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PostListens']))
		{
			$model->attributes=$_POST['PostListens'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->layout = 'normal-zamrudpoint';

		$themes = new Themes;
		$this->render('index',array(
			'themes'=>$themes,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PostListens('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PostListens']))
			$model->attributes=$_GET['PostListens'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PostListens the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PostListens::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PostListens $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='post-listens-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
/*
	public function actionPlan($theme, $topic)
	{
		$model = new ListenPlanForm;
		$model_lang = new Lang;
		$topic_details = new TopicDetails;
		$model_story = new StoryPointForm;

		$command=Yii::app()->db->createCommand(
			"SELECT name FROM themes WHERE id=:ID");
        $command->bindParam(":ID", $theme, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();

		$themename = $row['name'];

		$command=Yii::app()->db->createCommand(
			"SELECT name FROM topics WHERE id=:ID");
        $command->bindParam(":ID", $topic, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();

		$topicname = $row['name'];
		
		if (isset($_POST['ListenPlanForm']))
		{	
			 
			$model->attributes = $_POST['ListenPlanForm'];
			if ($model->validate())
			{ 
				$post = new PostListens;
				$post->attributes = $_POST['ListenPlanForm'];
				
				$post->date = date("Y-m-d G:i:s");
				$post->user_id = (Yii::app()->user->getId());
				$post->zamrud_points = 0;
				$post->status = 0;
				$post->theme_id = $theme;
				$post->topic_id = $topic;

				
				if ($post->save()) {
					$post->save(false); //no validation run
					
					//$model_detail = $_POST['PolymorphicForm'];
					foreach($model->detail as $cid){
  					  $details = new PostListenDetails;
			          $details->setIsNewRecord(true);  // To make sure that this would add new record and not update
			          $details->topic_detail_id = $cid;
			          $details->post_listen_id = $post->id;
			          $details->save();
					}


			       	$post_id = $post->id;
					$this->redirect(array('/postlistens/create','post_id'=>$post_id,'themename'=>$themename
						,'topicname'=>$topicname));
				} else {
					throw new CHttpException(405, CHtml::errorSummary($post));
				} 
			}
		}

		$this->render('plan',array('model'=>$model,'model_lang'=>$model_lang, 'topic_details'=>$topic_details, 
			'topic'=>$topic, 'themename'=>$themename,'topicname'=>$topicname,'model_story'=>$model_story));
	}
*/
	public function actionCreate($theme, $topic)
	{
		$this->layout = 'normal-zamrudpoint';
		
		$model_record = new ListenCreateForm;
		$model_lang = new Lang;

		$command=Yii::app()->db->createCommand(
			"SELECT name FROM themes WHERE id=:ID");
        $command->bindParam(":ID", $theme, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();

		$themename = $row['name'];

		$command=Yii::app()->db->createCommand(
			"SELECT name FROM topics WHERE id=:ID");
        $command->bindParam(":ID", $topic, PDO::PARAM_INT);
        $dataReader = $command->query();
        $row = $dataReader->read();
        $dataReader->close();

		$topicname = $row['name'];

		$max = Stories::model()->count('topic_id = :topic_id', array(':topic_id' => $topic));
		 
		$offset = rand(0,$max);
		 
		$object = Stories::model()->find(array(
		    'condition' => 'topic_id = :topic_id',
		    'params'    => array(':topic_id' =>  $topic),
		    'offset'    => $offset,
		    'limit'     => 1
		));

		$stories = $object->description;

  		if (isset($_POST['ListenCreateForm']))
		{	

			$model_record->attributes = $_POST['ListenCreateForm'];
			if ($model_record->validate())
			{ 
				$post = new PostListens;
				$post->attributes = $_POST['ListenCreateForm'];
				
				$post->date = date("Y-m-d G:i:s");
				$post->user_id = (Yii::app()->user->getId());
				$post->zamrud_points = 0;
				$post->status = 0;
				$post->theme_id = $theme;
				$post->topic_id = $topic;
				$post->story_id = $object->id;

				
				if ($post->save()) {
					$post->save(false); //no validation run
			       	$post_id = $post->id;
			       	$video = new VideoListens;
					$video->title = $post->title;
					$video->user_id = (Yii::app()->user->getId());
					$video->save(false);
					$video->post_listen_id = $post_id;

					$fileName = $video->id.'-'. (Yii::app()->user->getId()).'.webm';
					$pathFrom = Yii::app( )->getBasePath( )."/../uploads/temp/";
					$pathTo = Yii::app( )->getBasePath( )."/../uploads/video_listens/";
					rename($pathFrom.$_POST['ListenCreateForm']['upload_file'], $pathTo.$fileName);
					if( !is_dir( $pathFrom ) ) {
			            mkdir( $pathFrom );
			            chmod( $pathFrom, 0777 );
			        }
					$video->file = $fileName;

					$second = 2; //specify the time to get the screen shot at (can easily be randomly generated)
					$image = 'uploads/thumbnail_listens/'.$video->id.'-'. (Yii::app()->user->getId()).'.jpg';
					$videoname = 'uploads/video_listens/'.$fileName;
					//finally assemble the command and execute it

					
	                $cmd = "ffmpeg -i $videoname -deinterlace -an -ss $second -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $image 2>&1";

					shell_exec($cmd);
					
					$video->thumbnail = $video->id.'-'. (Yii::app()->user->getId()).'.jpg';
					$video->save(false);
					$video_id = $video->id;

					$model_post=PostListens::model()->findByPk((int)$post_id);
			        $model_post->status=1;
			        $model_post->save();
					$this->redirect(array('/site/index'));
					
				} else {
					throw new CHttpException(405, CHtml::errorSummary($post));
				} 
			}

		}
		

		$this->render('create',array('model_lang'=>$model_lang,'model_record'=>$model_record , 'stories'=>$stories));
	}

	public function actionStory()
	{
		echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
	}

}
