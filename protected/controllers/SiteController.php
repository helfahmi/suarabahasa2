<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
    {
        return array(
            'upload'=>array(
                'class'=>'xupload.actions.XUploadAction',
                'path' =>Yii::app() -> getBasePath() . "/../uploads",
                'publicPath' => Yii::app() -> getBaseUrl() . "/uploads",
            ),
            'page'=>array(
            	'class'=>'CViewAction',
            ),
        );
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/protected/extensions/videojs/assets/video.js',CClientScript::POS_BEGIN); 
		//Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/protected/extensions/videojs/assets/video-js.css'); 


		if (!Yii::app()->user->isGuest) {
			$this->layout = 'normal-zamrudpoint';
			$models = Posts::model()->findAll('status = :status', array(':status' => 1));
	  		$model = new Posts;
	  		$model_listens = PostListens::model()->findAll('status = :status', array(':status' => 1));
	  		$model_listen = new PostListens;
	  	
	  		//$model_listens = PostListens::model()->findAll('status = :status', array(':status' => 1));
	  		//$model_listen = new PostListens;

	  		$this->render('index', array('model'=>$model, 'models' => $models, 'model_listens'=>$model_listens, 'model_listen' => $model_listen));
		} else {
			$this->layout = 'home';
			$this->render('index');
		}

		
		/*	
		if (isset($_POST['RegisterForm']))
		{
			$model->attributes = $_POST['RegisterForm'];
			if ($model->validate())
			{
				$user = new User;
				$user->attributes = $_POST['RegisterForm'];
				
				$user->ttl = date("Y-m-d", strtotime($user->ttl));
				
				$user->salt = $user->generateSalt();
				$user->password = $user->hashPassword($user->password, $user->salt);
				$user->last_visit = date("Y-m-d G:i:s");
				$user->join_date = date("Y-m-d");

				$user->picture = CUploadedFile::getInstance($model, 'picture');
				
				if ($user->save()) {
					if ($user->picture) {
						$fileName = $user->username.'.'.$user->picture->extensionName;
						$path = getcwd().'/uploads/account/'.$fileName;
						$user->picture->saveAs($path);
						$user->picture = $fileName;
						$user->save(false); //no validation run
					}
					
					$this->redirect(array('/site/login'));
				} else {
					throw new CHttpException(405, CHtml::errorSummary($user));
				}
			}
		}
		*/

		
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	public function actionRegister(){
		$model = new RegisterForm;
		
		if (isset($_POST['RegisterForm']))
		{
			$model->attributes = $_POST['RegisterForm'];
			if ($model->validate())
			{
				$user = new User;
				$user->attributes = $_POST['RegisterForm'];
				
				/* Format tampilan beda sama format di database. Di tampilan: d-m-Y, di database Y-m-d. */
				$user->ttl = date("Y-m-d", strtotime($user->ttl));
				
				$user->salt = $user->generateSalt();
				$user->password = $user->hashPassword($user->password, $user->salt);
				$user->last_visit = date("Y-m-d G:i:s");
				$user->join_date = date("Y-m-d");
				$user->native_lang_id = 1;
				$user->learn_lang_id = 2;

				$user->picture = CUploadedFile::getInstance($model, 'picture');
				
				if ($user->save()) {
					if ($user->picture) {
						$fileName = $user->username.'.'.$user->picture->extensionName;
						$path = getcwd().'/uploads/account/'.$fileName;
						$user->picture->saveAs($path);
						$user->picture = $fileName;
						$user->save(false); //no validation run
					}
					
					$this->redirect(array('/site/login'));
				} else {
					throw new CHttpException(405, CHtml::errorSummary($user));
				}
			}
		}

		$this->render('register',array('model'=>$model));
	}


	/**
	 * Displays the login page
	 */
	public function actionCheckLogin(){
		if(Yii::app()->user->isGuest){
			echo "false";
		} else {
			echo "true";
		}
	}

	public function actionLogin()
	{	
		if(isset($_GET['fb']) && $_GET['fb'] == 1){
			$model = new LoginFormFB;

			$model->username = "user_fb_".$_POST['fb_id'];
			$model->fb_id = $_POST['fb_id'];

			//belum ada, bikin di database
			if(!$model->checkExist()){
				$user = new User;
				$user->full_name = $_POST['full_name'];
				$user->email = $_POST['email'];
				$user->gender = $_POST['gender'];
				$user->username = "user_fb_".$_POST['fb_id'];
				$user->password = 'Hehehe';
				$user->fb_id = $_POST['fb_id'];
				$user->last_visit = date("Y-m-d G:i:s");
				$user->join_date = date("Y-m-d");
				//default
				$user->native_lang_id = 1;
				$user->learn_lang_id = 2;
				$user->save();
			}

			if($model->login()){
				echo "Hey!";
				$this->redirect(array('site/index'));
			}
			echo "Hey2!";

		} else {
			$model=new LoginForm;

			// if it is ajax validation request
			if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}

			// collect user input data
			if(isset($_POST['LoginForm']))
			{
				$model->attributes=$_POST['LoginForm'];
				// validate user input and redirect to the previous page if valid
				if($model->validate() && $model->login())
					$this->redirect(Yii::app()->user->returnUrl);
			}
			// display the login form
			$this->render('login',array('model'=>$model));
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	/**
	 *  Displays the submission page
	 */
	public function actionCreate() 
	{
		//$this->render('create');
	}
	
	public function actionUpload() 
	{
		 echo '<script type="text/javascript"> alert(\'Hi\'); </script>';
	}

  /**
   *  Displays the correction page
   */
  public function actionCorrect()
  {
  	//harusnya jadi parameter
  	$post_id = 1;

  	$models = Advice::model()->findAll('post_id = :post_id', array(':post_id' => $post_id));
  	$model = new Advice;

  	if(isset($_POST['Advice'])){
		$model->attributes=$_POST['Advice'];
		unset($_POST['Advice']);
		
		$model->user_id = Yii::app()->user->id;
		$model->post_id = $post_id;
		$model->date = date("Y-m-d");
		$model->zamrud_points = 0;

		// validate user input and redirect to the previous page if valid
		if($model->save()){
			$this->redirect(array('site/correct')); //<-- harusnya jg pake post id lagi
		}
	}

    $this->render('correct', array('model'=>$model, 'models' => $models));
  }

	public function actionSave(){
		    // Muaz Khan         - www.MuazKhan.com
	    // MIT License       - www.WebRTC-Experiment.com/licence
	    // Documentation     - github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
	    
	    // make sure that you're using newest ffmpeg version!

	    // because we've different ffmpeg commands for windows & linux
	    // that's why following script is used to fetch target OS
	/*
	    $OSList = array
	    (
	    'Windows 3.11' => 'Win16',
	    'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)',
	    'Windows 98' => '(Windows 98)|(Win98)',
	    'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
	    'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
	    'Windows Server 2003' => '(Windows NT 5.2)',
	    'Windows Vista' => '(Windows NT 6.0)',
	    'Windows 7' => '(Windows NT 7.0)',
	    'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
	    'Windows ME' => 'Windows ME',
	    'Open BSD' => 'OpenBSD',
	    'Sun OS' => 'SunOS',
	    'Linux' => '(Linux)|(X11)',
	    'Mac OS' => '(Mac_PowerPC)|(Macintosh)',
	    'QNX' => 'QNX',
	    'BeOS' => 'BeOS',
	    'OS/2' => 'OS/2',
	    'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
	    );
	    // Loop through the array of user agents and matching operating systems
	    foreach($OSList as $CurrOS=>$Match)
	    {
	        // Find a match
	        if (preg_match($Match, $_SERVER['HTTP_USER_AGENT']))
	        {
	            // We found the correct match
	            break;
	        }
	    }
	*/

	    // if it is audio-blob
	    if (isset($_FILES["audio-blob"])) {
	        $uploadDirectory = 'uploads/temp/'.$_POST["filename"].'.wav';
	        if (!move_uploaded_file($_FILES["audio-blob"]["tmp_name"], $uploadDirectory)) {
	            echo("Problem writing audio file to disk!");
	        }
	        else {
	            // if it is video-blob
	            if (isset($_FILES["video-blob"])) {
	                $uploadDirectory = 'uploads/temp/'.$_POST["filename"].'.webm';
	                if (!move_uploaded_file($_FILES["video-blob"]["tmp_name"], $uploadDirectory)) {
	                    echo("Problem writing video file to disk!");
	                }
	                else {
	                    $audioFile = 'uploads/temp/'.$_POST["filename"].'.wav';
	                    $videoFile = 'uploads/temp/'.$_POST["filename"].'.webm';
	                    
	                    $mergedFile = 'uploads/temp/'.$_POST["filename"].'-merged.webm';
	                    
	                    // ffmpeg depends on yasm
	                    // libvpx depends on libvorbis
	                    // libvorbis depends on libogg
	                    // make sure that you're using newest ffmpeg version!
	                    
	                   // if(!strrpos($CurrOS, "Windows")) {
	                       $cmd = '-i '.$audioFile.' -i '.$videoFile.' -map 0:0 -map 1:0 '.$mergedFile;
	                 //   }
	                   /// else {
	                     //   $cmd = ' -i '.$audioFile.' -i '.$videoFile.' -c:v mpeg4 -c:a vorbis -b:v 64k -b:a 12k -strict experimental '.$mergedFile;
	                  //  }
	                    
	                    exec('ffmpeg '.$cmd.' 2>&1', $out, $ret);
	                    if ($ret){
	                        echo "There was a problem!\n";
	                        print_r($cmd.'\n');
	                        print_r($out);
	                    } else {
	                        echo "Ffmpeg successfully merged audi/video files into single WebM container!\n";
	                        
	                        unlink($audioFile);
	                        unlink($videoFile);
	                    }
	                }
	            }
	        }
	    }
	}
	
	public function actionDelete(){
		// Muaz Khan     - www.MuazKhan.com 
		// MIT License   - https://www.webrtc-experiment.com/licence/
		// Documentation - https://github.com/muaz-khan/WebRTC-Experiment/tree/master/RecordRTC
		if (isset($_POST['delete-file'])) {
		    $fileName = 'uploads/temp/'.$_POST['delete-file'];
		    if(!unlink($fileName.'-merged.webm') || !unlink($fileName.'.wav')) {
		        echo(' problem deleting files.');
		    }
		    else {
		        echo(' both wav/webm files deleted successfully.');
		    }
		}
	}

	public function actionChangeLang($lang){
		//echo $lang;
		Yii::app()->user->setState('_lang', $lang);
		$this->redirect(array('site/index'));
	}

	public function actionTest(){
		$this->render('videotest',array('nomor'=>5));
	}
}