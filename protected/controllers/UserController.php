<?php

class UserController extends Controller {
	public function actions() {
		//
	}

	public function actionFame() {
		$this->layout = 'normal-zamrudpoint';
		$this->render('fame');
	}

	public function actionProfile() {
		$this->layout = 'sidebar-zamrudpoint';
		$user_id = Yii::app()->user->getId();
		$model=User::model()->findByPk((int)$user_id);

		$modelposts = Posts::model()->findAll('status = :status AND user_id = :user_id', array(':status' => 1,':user_id' => $user_id));
	  	$modelpost = new Posts;

	  	$modelpostlistens = PostListens::model()->findAll('status = :status AND user_id = :user_id', array(':status' => 1,':user_id' => $user_id));
	  	$modelpostlisten = new PostListens;

		$this->render('profile',array('model'=>$model,'modelposts'=>$modelposts,'modelpost'=>$modelpost,'modelpostlistens'=>$modelpostlistens,'modelpostlisten'=>$modelpostlisten));
	}

	public function actionSetting() {
		$this->render('setting');
	}

	public function actionNotification() {
		$this->render('notification');
	}
}