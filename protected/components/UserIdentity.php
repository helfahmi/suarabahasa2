<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */

	private $_id;

	public static $status_error = 0;

	public function authenticateFacebook(){
		self::$status_error = 0;
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else {
			$this->_id=$user->id;
			$this->setState('name', $user->full_name);
			$this->setState('username', $this->username);
			$this->setState('fb_id', $user->fb_id);
			$this->setState('_lang', 'en');
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function authenticate()
	{
		self::$status_error = 0;
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else {
			$this->_id=$user->id;
			$this->setState('name', $user->full_name);
			$this->setState('username', $this->username);
			$this->setState('_lang', 'en');
			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId()
    {
        return $this->_id;
    }
}