<?php
	return array(
		// Panic
		'Credit' => 'Terima kasih',
		'Go to fame' => 'Daftar Peringkat',
		'Create Video for Practice Speaking' => 'Buat Video untuk Berlatih Bicara',
		'Create Video to Help Other Listening' => 'Buat Video untuk Membantu Teman Berlatih Mendengar',
		'What can you do with' => 'Apa yang dapat kamu lakukan dengan',
		'Tell your great stories in your target language' => 'Ceritakan kisah hebatmu dengan bahasa yang sedang kaupelajari',
		'Listen to your target language spoken by native speaker' => 'Dengarkan penutur asli berbicara bahasa yang sedang kaupelajari',
		'Share your advices with others to help them improving their skill' => 'Berbagi saran untuk membantu temanmu mempertajam kemampuannya',
		'Create listening quizes to help others learning your native language' => 'Buat kuis untuk membantu teman berlatih mendengar',
		'Practice Speaking' => 'Berlatih Bicara',
		'Practice Listening' => 'Berlatih Mendengar',
		'Help Others Speaking' => 'Bantu Teman Berbicara',
		'Help Others Listening' => 'Bantu Teman Mendengar',

		// General
		'About' => 'Tentang',
		'By Advice' => 'Dengan Saran',
		'By Practice' => 'Dengan Latihan',
		'change' => 'ubah',
		'Choose your language' => 'Pilih bahasa yang Anda inginkan',
		'Close' => 'Tutup',
		'Create an exercise' => 'Buat latihan',
		'Help' => 'Bantuan',
		'Help Other' => 'Bantu Teman',
		'Hi' => 'Hai',
		'Listening' => 'Mendengar',
		'Log In' => 'Masuk',
		'Log Out' => 'Keluar',
		'Notification' => 'Notifikasi',
		'Practice' => 'Berlatih',
		'Reading' => 'Membaca',
		'See more' => 'Lihat semuanya',
		'Setting' => 'Pengaturan',
		'Speaking' => 'Berbicara',
		'Writing' => 'Menulis',

		// Zamrud point
		'Zamrud Point' => 'Poin Zamrud',

		// Log in home
		'Already has account?' => 'Sudah memiliki akun?',
		'boosts your language skills.' => 'meningkatkan kemampuan berbahasa Anda.',
		'Create new' => 'Buat baru',
		'Log in here' => 'Masuk di sini',
		'Log in with' => 'Masuk dengan',
		'or' => 'atau',
		'Register' => 'Daftar',

		// Log in
		'remember me' => 'ingat saya',

		// Register
		'Birth' => 'Tanggal Lahir',
		'characters.' => 'karakter.',
		'Email' => 'Email',
		'Max.' => 'Maksimum',
		'Full Name' => 'Nama Panjang',
		'Password' => 'Password',
		'Profile Picture' => 'Foto Profil',
		'Registration' => 'Pendaftaran',
		'Repeat Password' => 'Ulangi Password',
		'Username' => 'Username',

		// Create video (speaking)
		'Create' => 'Buat',
		'Create a Great Video!' => 'Buat Video yang Hebat!',
		'Content' => 'Konten',
		'Correct' => 'Koreksi',
		'current' => 'sekarang',
		'Delete' => 'Hapus',
		'Description' => 'Deskripsi',
		'Language' => 'Bahasa',
		'New' => 'Baru',
		'Draft' => 'Draf',
		'Drafts' => 'Draf',
		'Recent' => 'Yang lalu',
		'Recents' => 'Yang lalu',
		'Record' => 'Rekam',
		'Record now!' => 'Rekam sekarang!',
		'Record Your Video Now!' => 'Rekam Videomu Sekarang!',
		'Save' => 'Simpan',
		'Save & Give Tag' => 'Simpan & Beri Tag',
		'Stop' => 'Stop',
		'Title' => 'Judul', 
		'Video File' => 'File Video',

		// Newsfeed
		'advice' => 'saran',
		'by' => 'oleh',
		'like' => 'suka',
		'Mark as spam' => 'Tandai sebagai spam',
		'More' => 'Menu',
		'Newsfeed' => 'Beranda',
		'No description yet' => 'Belum ada deskripsi',
		'watch' => 'tonton',

		// Profile page
		'Change picture' => 'Ganti gambar',
		'Hobby' => 'Hobi',
		'My Advice' => 'Saranku',
		'My Video' => 'Videoku',
		'Native language' => 'Bahasa asal',
		'No advice yet' => 'Belum ada saran',
		'Profile' => 'Profil',
		'see all' => 'semua',
		'Target language' => 'Bahasa tujuan',

		// About
		'zamrud about' => 'How could it be?',
	);
?>