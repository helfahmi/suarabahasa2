<?php
	return array(
		'boosts your spoken language experience practically.' => 'meningkatkan pengalaman berbicara Anda secara nyata.',
		'Tell your great stories in your target language.' => 'Ceritakan kisah hebatmu dalam bahasa yang sedang kaupelajari.',
		'Listen to your target language spoken by native speaker.' => 'Dengarkan bahasa yang sedang kaupelajari langsung dari penutur aslinya.',
		'Share your advices with others to help them improving their skill.' => 'Berbagi kiat-kiatmu dengan yang lain untuk membantu mereka meningkatkan kemampuan mereka.',
		'Create listening quizes to help others learning your native language.' => 'Buat kuis mendengar untuk membantu yang lain berlatih bahasa daerahmu.',
	);
?>