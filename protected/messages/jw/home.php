<?php
	return array(
		'boosts your spoken language experience practically.' => 'ngedongkrak ngandika basa pengalaman sacoro prakteke.',
		'Tell your great stories in your target language.' => 'Marang gedhe crita ing basa target.',
		'Listen to your target language spoken by native speaker.' => 'Ngrungokake basa target ngandika dening native speaker.',
		'Share your advices with others to help them improving their skill.' => 'Nuduhake advices karo liyane kanggo bantuan mau nambah skill.',
		'Create listening quizes to help others learning your native language.' => 'Nggawe ngrungokake Mrikso bantuan liyane learning native language.',
	);
?>