<?php
	return array(
		'Log In' => 'Masuk',
		'Register' => 'Daftar',
		'Register now!' => 'Daftar sekarang!',
		'Registration' => 'Pendaftaran',
		'remember me' => 'ingat saya',
		'Username' => 'Username',
		'Password' => 'Password',
		'Repeat Password' => 'Ulangi Password',
		'Full Name' => 'Nama Lengkap',
		'Profile Picture' => 'Foto Profil',
		'Birth' => 'TTL',
		'Email' => 'Email'
	);
?>