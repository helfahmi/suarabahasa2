# Suara Bahasa (**ZAMRUD**) #
*where lang.aholics rise their voices together*

## Description ##
An application made using PHP with Yii framework & Bootstrap layout. Made for *INAICTA2014*.

## Setup ##
* yii folder (http://www.yiiframework.com/download/) inside htdocs
* This project folder inside htdocs
* Create MySQL DB : 'zamrud'
* Import 'zamrud.sql'

## How to run ##
* Start server (Apache, MySQL)
* open browser: localhost/suarabahasa2

## Contributor ##
* [044](https://bitbucket.org/044)
* [helfahmi](https://bitbucket.org/helfahmi)
* [alifanuraniputri](https://bitbucket.org/alifanuraniputri)