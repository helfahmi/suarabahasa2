-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Sep 2014 pada 11.10
-- Versi Server: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zamrud`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `advices`
--

CREATE TABLE IF NOT EXISTS `advices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `content` text,
  `zamrud_points` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `upvote` int(11) DEFAULT NULL,
  `downvote` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_INDEX` (`user_id`),
  KEY `post_INDEX` (`post_id`),
  KEY `parent_INDEX` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `castes`
--

CREATE TABLE IF NOT EXISTS `castes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `req_zamrud` int(11) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `langs`
--

CREATE TABLE IF NOT EXISTS `langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `langs`
--

INSERT INTO `langs` (`id`, `code`, `name`) VALUES
(1, 'id', 'Bahasa Indonesia'),
(2, 'en', 'English'),
(5, 'jw', 'Jawa'),
(6, 'snd', 'Sunda'),
(7, 'btk', 'Batak'),
(8, 'mng', 'Minang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  KEY `sender_INDEX` (`sender_id`),
  KEY `receiver_INDEX` (`receiver_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `date` varchar(45) DEFAULT NULL,
  `zamrud_points` int(11) DEFAULT '0',
  `lang_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0',
  `dislikes` int(11) NOT NULL,
  `watchs` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_INDEX` (`user_id`),
  KEY `lang_INDEX` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `content`, `date`, `zamrud_points`, `lang_id`, `status`, `likes`, `dislikes`, `watchs`) VALUES
(32, 8, 'Aku nggak jelas', '<p>sdsdsd</p>', '2014-09-11 9:44:22', 0, 1, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_listens`
--

CREATE TABLE IF NOT EXISTS `post_listens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `story_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `zamrud_points` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  `dislikes` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `post_listens`
--

INSERT INTO `post_listens` (`id`, `user_id`, `lang_id`, `theme_id`, `topic_id`, `story_id`, `title`, `zamrud_points`, `date`, `status`, `likes`, `dislikes`) VALUES
(2, 9, 1, 4, 8, 7, 'Test 2', 0, '2014-09-09', 1, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_id` int(11) NOT NULL,
  `question` varchar(200) NOT NULL,
  `answer1` varchar(100) NOT NULL,
  `answer2` varchar(100) NOT NULL,
  `answer3` varchar(100) NOT NULL,
  `answer4` varchar(100) NOT NULL,
  `answer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data untuk tabel `questions`
--

INSERT INTO `questions` (`id`, `story_id`, `question`, `answer1`, `answer2`, `answer3`, `answer4`, `answer`) VALUES
(1, 1, 'Apa Bahan Utama Gudeg ?', 'Telur dan Santan', 'Nangka Tua dan Cabe', 'Nangka Muda dan Santan', 'Cabe dan Kacang', 3),
(2, 1, 'Mana yang tidak termasuk jenis Gudeg?', 'Gudeg Basah', 'Gudeg Goreng', 'Gudeg Kering', 'Gudeg Putih', 2),
(3, 1, 'Dari mana asal kata Gudeg?\r\n', '"Good" & "Dhek"', '"Good" & "Deck"', '"Goo" & "Dhek"', '"Goo" & "Deck"', 1),
(4, 1, 'Manakah yang termasuk makanan sampingan Gudeg?', 'Sambel Ijo', 'Sambel Goreng Krecek', 'Sambel Goreng Ati', 'Sambel Kacang', 2),
(5, 2, 'Apakah rasa utama Rendang?', 'Manis', 'Pedas', 'Asam', 'Gurih', 2),
(6, 2, 'Dari provinsi mana Rendang Berasal?', 'Sulawesi Barat', 'Sulawesi Utara', 'Sumatera Barat', 'Sumatera Utara', 3),
(7, 2, 'Berapa rata-rata waktu yang dihabiskan untuk memasak Rendang ?', '5 jam', '4 jam', '3 jam', '2 jam', 2),
(8, 2, 'Apa warna Kalio ?', 'Sama seperti Rendang', 'Coklat Kekmerahan', 'Keemasan', 'Coklat muda', 3),
(9, 3, 'Apa bahan utama Pempek ?', 'Ayam dan Sagu', 'Ayam dan Terigu', 'Ikan dan Terigu', 'Ikan dan Sagu', 4),
(10, 3, 'Mana yang tidak termasuk Jenis Pempek ?', 'Pempek Siomay', 'Pepek Kapal Selam', 'Pempek Keriting', 'Pempek Kulit', 1),
(11, 3, 'Apa rasa khas Cuko pada awalnya?', 'Manis', 'Pedas', 'Pahit', 'Asam', 2),
(12, 3, 'Cuko dapat bermanfaat bagi ...', 'Mulut', 'Lambung', 'Gigi', 'Pencernaan', 3),
(13, 4, 'Dari mana Kerak tlur berasal?', 'Betawi', 'Banten', 'Solo', 'Yogyakarta', 1),
(14, 4, 'Berapa lama kira-kira kerak telur dimasak?', '7 menit', '6 menit', '5 menit', '4 menit', 3),
(15, 4, 'Apa salah satu bahan dasar kerak telur ?', 'Ketan hitam', 'Ketam putih', 'Ketan merah', 'Ketang beras', 2),
(16, 4, 'Bagaimana kerak telur dikemas?', 'Dibunkus daun', 'Dilipat', 'Digulung', 'Dipotong', 3),
(17, 7, 'Manakah yang termasuk varian batagor?', 'telur', 'kol', 'pangsit', 'siomay', 4),
(18, 4, 'Apa salah satu bahan dasar batagor?', 'Ikan bandeng', 'Tepung tapioka', 'Tepung beras', 'Ikan Salem', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `quizes`
--

CREATE TABLE IF NOT EXISTS `quizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_listen_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `stories`
--

INSERT INTO `stories` (`id`, `topic_id`, `description`) VALUES
(1, 8, 'Apakah Anda tahu Gudeg ?\n\nGudeg merupakan makanan khas Yogyakarta yang terbuat dari nangka muda dan santan.\n\nWarna coklat pada Gudeg dihasilkan oleh daun Jati.\n\nAda tiga jenis gudeg : Gudeg kering, Gudeg Basah, dan Gudeg Solo(Gudeg Putih).\n\nMenurut Cerita, Gudeg berasal dari 2 kata : "good" dan "dhek", yang dikemukakan orang belanda ke istrinya untuk memuji masakannya pada jaman dahulu.\n\nMemasak Gudeg sangatlah rumit, dan menghabiskan waktu berjam-jam.\n\nGudeg Dimakan dengan nasi dan disajikan dengan areh (kuah santan), sambel goreng krecek, dan aneka lauk nya.\n\nItulah cerita tentang Gudeg!'),
(2, 8, 'Apakah anda sudah pernah makan rendang?\n\nRendang merupakan masakan daging pedas yang menggunakan campuran rempah-rempah.\n\nRendang berasal dari Minangkabau, Sumatera Barat.\n\nMemasak endang sangat lama, bisa menghabiskan sekitar 4 jam.\n\nRendang bisa bertahan hingga berminggu-minggu.\n\nJika tidak mau memasak lama, ada makanan sejenis rendang yang dimasak dalam waktu yang lebih singkat bernama Kalio, warnanya keemasan.\n\nRendang pernah meraih peringkat hidangan pertama di World''s 50 Most Delicious Food 2011.\n'),
(3, 8, 'Pempek? Pasti Anda sudah pernah mencoba ya?\n\nPempek merupakan makanan khas Palemang, Sumatera Selatan.\n\nPempek terbuat dari bahan utama ikan dan Sagu.\n\nAda beberapa jenis Pembpek antara lain : Kapal Selam, Lenjer, Kulit, Ada''an, Keriting, dll.\n\nPempek disajikan dengan saos berwarna hitam kecoklat-coklatan yang disebut Cuko.\n\nPada awalnya Cuko selalu dibuat pedas untuk menambah nafsu makan.\n\nAda hal menarik tentang Cuko, Cuko bisa melindungi gigi dari Karies.'),
(4, 8, 'Kerak telor ? Percayalah makanan ini enak walau bernama kerak.\n\nKerak telor merupakan makanan asli Betawi alias Jakarta.\n\nBahan dasar yang digunakan yaitu beras ketan putih, telur, ayam, ebi, kelapa sangrai dan tenteunya telur.\n\nKerak telur selalu dimasak satu per satu dengan kompor areng.\n\nProses pembuatannya cukup rumit, namun tidak menghabiskan waktu lama, hanya sekitar 5 menit.\n\nSetelah matang, kerak telur dikemas dengan digulung.'),
(7, 8, 'Batagor, makanan yang sangat mudah ditemui di Indonesia.\n\nBatagor merupakan makanan khas Bandung, Jawa Barat. \n\nBatagor merupakan singkatan dari bakso tahu goreng.\n\nBatagor mengadaptasi gaya Tionghoa-Indonesia.\n\nBahan dasar Batagor adalah tahu, ikan tengiri dan tepung tapioka.\n\nAda dua variasai yaitu siomay goreng dan batagor itu sendiri.\n\nBatagor disajikan dengan bumbu kacang, kecap manis, dan sambal.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_id` int(11) NOT NULL,
  `time_tag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `tags`
--

INSERT INTO `tags` (`id`, `video_id`, `time_tag`) VALUES
(2, 2, 0),
(3, 2, 10),
(4, 2, 20),
(5, 3, 0),
(6, 3, 6),
(7, 3, 17),
(8, 3, 24),
(9, 4, 0),
(10, 4, 14),
(11, 4, 17),
(12, 5, 0),
(13, 5, 2),
(14, 5, 0),
(15, 5, 1),
(16, 5, 2),
(17, 5, 3),
(18, 10, 0),
(19, 28, 0),
(20, 28, 1),
(21, 28, 2),
(22, 29, 0),
(23, 29, 7),
(24, 29, 61),
(25, 29, 84),
(26, 30, 0),
(27, 30, 17),
(28, 30, 27),
(29, 31, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `image_path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `themes`
--

INSERT INTO `themes` (`id`, `name`, `image_path`) VALUES
(4, 'Traditional Food', ''),
(5, 'Local Art', ''),
(6, 'Folklore', ''),
(7, 'Geography', ''),
(8, 'Politic', ''),
(9, 'Sport', ''),
(10, 'History', ''),
(11, 'Tourism', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image_path` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data untuk tabel `topics`
--

INSERT INTO `topics` (`id`, `theme_id`, `name`, `image_path`) VALUES
(7, 4, 'Indonesian Drink', ''),
(8, 4, 'Indonesian Food', ''),
(9, 5, 'Traditional Music', ''),
(10, 5, 'Traditional Dance', ''),
(11, 5, 'Traditional Craft', ''),
(12, 6, 'Legend', ''),
(13, 6, 'Myth', ''),
(14, 7, 'Island', ''),
(15, 7, 'Mountain', ''),
(16, 7, 'Sea & River', ''),
(18, 8, 'Presiden', ''),
(19, 8, 'Government', ''),
(21, 9, 'Atlet', ''),
(23, 9, 'Achievement', ''),
(24, 10, 'Hero', ''),
(25, 10, 'Palace', ''),
(26, 10, 'Organizations', ''),
(27, 11, 'Place', ''),
(28, 11, 'Souvenir', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `ttl` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `salt` varchar(45) DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `green_zamrud` int(11) DEFAULT '0',
  `red_zamrud` int(11) DEFAULT '0',
  `disciple_caste_id` int(11) DEFAULT NULL,
  `master_caste_id` int(11) DEFAULT NULL,
  `native_lang_id` int(11) DEFAULT NULL,
  `learn_lang_id` int(11) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `fb_id` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `disc_caste_INDEX` (`disciple_caste_id`),
  KEY `native_INDEX` (`native_lang_id`),
  KEY `learn_INDEX` (`learn_lang_id`),
  KEY `master_caste_INDEX` (`master_caste_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `full_name`, `name`, `ttl`, `email`, `username`, `password`, `salt`, `last_visit`, `join_date`, `picture`, `green_zamrud`, `red_zamrud`, `disciple_caste_id`, `master_caste_id`, `native_lang_id`, `learn_lang_id`, `role`, `fb_id`, `gender`) VALUES
(4, 'harits elfahmi', NULL, '2014-07-08', 'adil@gmail.com', 'harits', 'f9d56a47a8929e7f7f5caf4030a83bb3', '53a1756ddb2938.13494311', '2014-06-18 13:18:05', '2014-06-18', 'harits.jpg', 0, 0, NULL, NULL, NULL, NULL, NULL, '', ''),
(5, 'fathan pranaya', NULL, '1994-04-27', 'fathanpranaya@gmail.com', 'fathan', '3bd2af818daefb8ef99c40ede4b0958c', '53b612c74ade50.10155694', '2014-07-04 04:34:47', '2014-07-04', NULL, 0, 0, NULL, NULL, 1, 2, NULL, '', ''),
(6, 'Alifa Nurani Putri', NULL, '1994-03-05', 'alifa@gmail.com', 'alifaaa', '02c32d7b6c1fca5d85c6a4525f39c4eb', '53b7a1f84f6d17.76595087', '2014-07-05 08:58:00', '2014-07-05', 'alifaaa.jpg', 0, 0, NULL, NULL, 1, 2, NULL, '', ''),
(8, 'Muhammad Harits Elfahmi', NULL, NULL, 'adilelfahmi@gmail.com', 'user_fb_493506250780275', 'Hehehe', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, '493506250780275', 'male'),
(9, 'Alifa Nurani Putri', NULL, '1994-03-05', 'alifanuraniputri@gmail.com', 'user_fb_10202533126351641', 'Hehehe', NULL, '2014-07-06 09:02:58', '2014-07-06', 'alifanuraniputri.jpg', 0, 0, NULL, NULL, 1, 2, NULL, '10202533126351641', 'female'),
(10, 'undefined', NULL, NULL, 'undefined', 'user_fb_undefined', 'Hehehe', NULL, '2014-07-31 12:51:15', '2014-07-31', NULL, 0, 0, NULL, NULL, 1, 2, NULL, 'undefined', 'undefined');

-- --------------------------------------------------------

--
-- Struktur dari tabel `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(50) NOT NULL,
  `file` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_INDEX` (`user_id`),
  KEY `post_INDEX` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data untuk tabel `videos`
--

INSERT INTO `videos` (`id`, `title`, `user_id`, `post_id`, `thumbnail`, `file`) VALUES
(31, 'Aku nggak jelas', 8, 32, '31-8.jpg', '31-8.webm');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video_listens`
--

CREATE TABLE IF NOT EXISTS `video_listens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_listen_id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `thumbnail` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `video_listens`
--

INSERT INTO `video_listens` (`id`, `title`, `user_id`, `post_listen_id`, `file`, `thumbnail`) VALUES
(2, 'Test 2', 9, 2, '2-9.mp4', '2-9.jpg');

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `advices`
--
ALTER TABLE `advices`
  ADD CONSTRAINT `comment_comments` FOREIGN KEY (`parent_id`) REFERENCES `advices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comment_posts` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comment_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `receiver_user` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `sender_user` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `lang_posts` FOREIGN KEY (`lang_id`) REFERENCES `langs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_posts` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `learn_lang` FOREIGN KEY (`learn_lang_id`) REFERENCES `langs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `native_lang` FOREIGN KEY (`native_lang_id`) REFERENCES `langs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_disc_caste` FOREIGN KEY (`disciple_caste_id`) REFERENCES `castes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_master_caste` FOREIGN KEY (`master_caste_id`) REFERENCES `castes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `users_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_video` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
